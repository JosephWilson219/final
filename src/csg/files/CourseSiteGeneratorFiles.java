package csg.files;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorPropertyType.csg_schedule_friday_date;
import static csg.CourseSiteGeneratorPropertyType.csg_schedule_monday_date;
import static csg.CourseSiteGeneratorPropertyType.csg_site_css_combo;
import static csg.CourseSiteGeneratorPropertyType.csg_site_email_textfield;
import static csg.CourseSiteGeneratorPropertyType.csg_site_exportVal_label;
import static csg.CourseSiteGeneratorPropertyType.csg_site_export_dir_warning;
import static csg.CourseSiteGeneratorPropertyType.csg_site_favicon_image;
import static csg.CourseSiteGeneratorPropertyType.csg_site_home_checkbox;
import static csg.CourseSiteGeneratorPropertyType.csg_site_homepage_textfield;
import static csg.CourseSiteGeneratorPropertyType.csg_site_hws_checkbox;
import static csg.CourseSiteGeneratorPropertyType.csg_site_leftFooter_image;
import static csg.CourseSiteGeneratorPropertyType.csg_site_name_textfield;
import static csg.CourseSiteGeneratorPropertyType.csg_site_navbar_image;
import static csg.CourseSiteGeneratorPropertyType.csg_site_number_combo;
import static csg.CourseSiteGeneratorPropertyType.csg_site_oh_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_site_rightFooter_image;
import static csg.CourseSiteGeneratorPropertyType.csg_site_room_textfield;
import static csg.CourseSiteGeneratorPropertyType.csg_site_schedule_checkbox;
import static csg.CourseSiteGeneratorPropertyType.csg_site_semester_combo;
import static csg.CourseSiteGeneratorPropertyType.csg_site_subject_combo;
import static csg.CourseSiteGeneratorPropertyType.csg_site_syllabus_checkbox;
import static csg.CourseSiteGeneratorPropertyType.csg_site_title_textfield;
import static csg.CourseSiteGeneratorPropertyType.csg_site_year_combo;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_academicDishonesty_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_description_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_gradedComponents_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_gradingNote_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_outcomes_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_prereqs_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_specialAssistance_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_textbooks_textarea;
import static csg.CourseSiteGeneratorPropertyType.csg_syllabus_topics_textarea;
import csg.data.CourseSiteGeneratorData;
import static csg.data.CourseSiteGeneratorData.syllabusItems.AcademicDishonesty;
import static csg.data.CourseSiteGeneratorData.syllabusItems.Description;
import static csg.data.CourseSiteGeneratorData.syllabusItems.GradedComponents;
import static csg.data.CourseSiteGeneratorData.syllabusItems.GradingNote;
import static csg.data.CourseSiteGeneratorData.syllabusItems.Outcomes;
import static csg.data.CourseSiteGeneratorData.syllabusItems.Preqs;
import static csg.data.CourseSiteGeneratorData.syllabusItems.SpecialAssistance;
import static csg.data.CourseSiteGeneratorData.syllabusItems.Textbooks;
import static csg.data.CourseSiteGeneratorData.syllabusItems.Topics;
import csg.data.MeetingTime;
import csg.data.ScheduleItem;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import csg.workspace.CourseSiteGeneratorWorkspace;
import djf.modules.AppGUIModule;
import djf.ui.AppNodesBuilder;
import djf.ui.dialogs.AppWebDialog;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class CourseSiteGeneratorFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    CourseSiteGeneratorApp app;
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_NAME = "name";
    static final String JSON_EMAIL = "email";
    static final String JSON_TYPE = "type";
    static final String JSON_SLOTS = "timeSlot";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_SITE = "SiteTab";
    static final String JSON_SYLLABUS = "Syllabus";
    static final String JSON_MEETING = "Meeting";
    static final String JSON_PROGRAM = "programData";
    
    static final String JSON_PROGRAM_SUB_INT = "subInt";
    static final String JSON_PROGRAM_SUB_LIST = "subList";
    static final String JSON_PROGRAM_NUM_INT = "numInt";
    static final String JSON_PROGRAM_NUM_LIST = "numList";
    static final String JSON_PROGRAM_YEAR_INT = "yearInt";
    static final String JSON_PROGRAM_YEAR_LIST = "yearList";
    static final String JSON_PROGRAM_SEM_INT = "semInt";
    static final String JSON_PROGRAM_SEM_LIST = "semList";
    static final String JSON_PROGRAM_CSS = "css";
    
    static final String JSON_MEETING_LECS = "lectures";
    static final String JSON_MEETING_RECS = "recitations";
    static final String JSON_MEETING_LABS = "labs";
    static final String JSON_MEETING_SECTION = "section";
    static final String JSON_MEETING_DAYS = "days";
    static final String JSON_MEETING_TIME = "time";
    static final String JSON_MEETING_DAYSTIME = "day_time";
    static final String JSON_MEETING_LOCATION = "room";
    static final String JSON_MEETING_TA_ONE = "ta_1";
    static final String JSON_MEETING_TA_TWO = "ta_2";
    
    static final String JSON_SITE_SUB = "subject";
    static final String JSON_SITE_NUM = "number";
    static final String JSON_SITE_SEM = "semester";
    static final String JSON_SITE_YEAR = "year";
    static final String JSON_SITE_TITLE = "title";
    static final String JSON_SITE_PAGES = "pages";
    static final String JSON_SITE_PAGES_NAME = "name";
    static final String JSON_SITE_PAGES_LINK = "link";
    static final String JSON_SITE_HOME = "Home";
    static final String JSON_SITE_HOME_LINK = "index.html";
    static final String JSON_SITE_SYLLABUS = "Syllabus";
    static final String JSON_SITE_SYLLABUS_LINK = "syllabus.html";
    static final String JSON_SITE_SCHEDULE = "Schedule";
    static final String JSON_SITE_SCHEDULE_LINK = "schedule.html";
    static final String JSON_SITE_HWS = "HWs";
    static final String JSON_SITE_HWS_LINK = "hws.html";
    static final String JSON_SITE_LOGOS = "logos";
    static final String JSON_SITE_FAV = "favicon";
    static final String JSON_SITE_NAV = "navbar";
    static final String JSON_SITE_LEFT = "bottom_left";
    static final String JSON_SITE_RIGHT = "bottom_right";
    static final String JSON_SITE_INSTRUCTOR = "instructor";
    static final String JSON_SITE_NAME = "name";
    static final String JSON_SITE_PAGE = "link";
    static final String JSON_SITE_ROOM = "room";
    static final String JSON_SITE_EMAIL = "email";
    static final String JSON_SITE_PHOTO = "photo";
    static final String JSON_SITE_HOURS = "hours";
    
    static final String JSON_SYLLABUS_DESCRIPTION ="description";
    static final String JSON_SYLLABUS_TOPICS = "topics";
    static final String JSON_SYLLABUS_PREQ = "prerequisites";
    static final String JSON_SYLLABUS_OUTCOMES = "outcomes";
    static final String JSON_SYLLABUS_TEXTBOOKS ="textbooks";
    static final String JSON_SYLLABUS_GRADED = "gradedComponents";
    static final String JSON_SYLLABUS_NOTE ="gradingNote";
    static final String JSON_SYLLABUS_DISHONESTY = "academicDishonesty";
    static final String JSON_SYLLABUS_ASSISTANCE ="specialAssistance";  
    
    static final String JSON_SCHEDULE_MONDAY_MONTH = "startingMondayMonth";
    static final String JSON_SCHEDULE_MONDAY_DAY = "startingMondayDay";
    static final String JSON_SCHEDULE_FRIDAY_MONTH = "endingFridayMonth";
    static final String JSON_SCHEDULE_FRIDAY_DAY = "endingFridayDay";
    static final String JSON_SCHEDULE_MONTH = "month";
    static final String JSON_SCHEDULE_DAY = "day";
    static final String JSON_SCHEDULE_TITLE = "title";
    static final String JSON_SCHEDULE_TOPIC = "topic";
    static final String JSON_SCHEDULE_LINK = "link";
    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_SCHEDULE_HOLIDAYS = "holidays";
    static final String JSON_SCHEDULE_LECTURES= "lectures";
    static final String JSON_SCHEDULE_HWS = "hws";
    static final String JSON_SCHEDULE_RECS = "recitations";
    static final String JSON_SCHEDULE_REFS = "references";
    
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_START_TIME = "time";
    static final String JSON_DAY_OF_WEEK = "day";
    static final String JSON_MONDAY = "monday";
    static final String JSON_TUESDAY = "tuesday";
    static final String JSON_WEDNESDAY = "wednesday";
    static final String JSON_THURSDAY = "thursday";
    static final String JSON_FRIDAY = "friday";

    public CourseSiteGeneratorFiles(CourseSiteGeneratorApp initApp) {
        app = initApp;
    }

    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        AppGUIModule gui = app.getGUIModule();
	// CLEAR THE OLD DATA OUT
	CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData)data;
        dataManager.reset();

	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject wholeJson = loadJSONFile(filePath);
        JsonObject officeHoursJson = wholeJson.getJsonObject(JSON_OFFICE_HOURS);

	// LOAD THE START AND END HOURS
	String startHour = officeHoursJson.getString(JSON_START_HOUR);
        String endHour = officeHoursJson.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);

        // NOW LOAD ALL THE UNDERGRAD TAs
        JsonArray jsonTAArray = officeHoursJson.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            String type = "undergraduate";
            //String slots = jsonTA.getString(JSON_SLOTS);
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name, email, "0",type);
            dataManager.addTA(ta);
        }
        //now graduates
        jsonTAArray = officeHoursJson.getJsonArray(JSON_GRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            String type = "graduate";
            //String slots = jsonTA.getString(JSON_SLOTS);
            TeachingAssistantPrototype ta = new TeachingAssistantPrototype(name, email, "0",type);
            dataManager.addTA(ta);
        }
        dataManager.switchToType("all");
        
        JsonArray jsoncsgArray = officeHoursJson.getJsonArray(JSON_OFFICE_HOURS);
        
        
        //timeslots
        for (int i = 0; i < jsoncsgArray.size(); i++) {
            JsonObject jsonT = jsoncsgArray.getJsonObject(i);
            String time= jsonT.getString(JSON_START_TIME).replace(":", "_");
            String day = jsonT.getString(JSON_DAY_OF_WEEK);
            String name = jsonT.getString(JSON_NAME);
            TimeSlot curr = dataManager.getTimeSlot(time);
            TeachingAssistantPrototype ta = dataManager.getTAWithName(name);
            int slots = Integer.parseInt(ta.getTimeslot());
            ta.setTimeslot(""+(slots));
            day = day.toLowerCase();
            dataManager.addToTimeslot(ta, day, curr);
        }
        
        JsonObject meetingTimeObj = wholeJson.getJsonObject(JSON_MEETING);
        
        JsonArray jsonLecArray = meetingTimeObj.getJsonArray(JSON_MEETING_LECS);
        for (int i = 0; i < jsonLecArray.size(); i++) {
            JsonObject jsonT = jsonLecArray.getJsonObject(i);
            String section= jsonT.getString(JSON_MEETING_SECTION);
            String days = jsonT.getString(JSON_MEETING_DAYS);
            String time = jsonT.getString(JSON_MEETING_TIME);
            String location = jsonT.getString(JSON_MEETING_LOCATION);
            MeetingTime meetingTime = new MeetingTime(section, days, time, " ", "Lecture", location, " ", " ");
            dataManager.addLec(meetingTime);
        }
        JsonArray jsonRecArray = meetingTimeObj.getJsonArray(JSON_MEETING_RECS);
        for (int i = 0; i < jsonRecArray.size(); i++) {
            JsonObject jsonT = jsonRecArray.getJsonObject(i);
            String section= jsonT.getString(JSON_MEETING_SECTION);
            String daysAndTime = jsonT.getString(JSON_MEETING_DAYSTIME);
            String location = jsonT.getString(JSON_MEETING_LOCATION);
            String taOne = jsonT.getString(JSON_MEETING_TA_ONE);
            String taTwo = jsonT.getString(JSON_MEETING_TA_TWO);
            MeetingTime meetingTime = new MeetingTime(section, " ", " ", daysAndTime, "Reciation", location, taOne,taTwo);
            dataManager.addRec(meetingTime);
        }
        JsonArray jsonLabArray = meetingTimeObj.getJsonArray(JSON_MEETING_LABS);
        for (int i = 0; i < jsonLabArray.size(); i++) {
            JsonObject jsonT = jsonLabArray.getJsonObject(i);
            String section= jsonT.getString(JSON_MEETING_SECTION);
            String daysAndTime = jsonT.getString(JSON_MEETING_DAYSTIME);
            String location = jsonT.getString(JSON_MEETING_LOCATION);
            String taOne = jsonT.getString(JSON_MEETING_TA_ONE);
            String taTwo = jsonT.getString(JSON_MEETING_TA_TWO);
            MeetingTime meetingTime = new MeetingTime(section, " ", " ", daysAndTime, "Lab", location, taOne,taTwo);
            dataManager.addLab(meetingTime);
        }
        
        //Site tab
        
        JsonObject site = wholeJson.getJsonObject(JSON_SITE);
        JsonObject programData = wholeJson.getJsonObject(JSON_PROGRAM);
        //banner section
        ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getItems().clear();
        JsonArray subList = programData.getJsonArray(JSON_PROGRAM_SUB_LIST);
        for (int i =0; i<subList.size(); i++) {
            ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getItems().add(subList.get(i).toString().replaceAll("\"" , ""));
        }
        ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getSelectionModel().select(programData.getInt(JSON_PROGRAM_SUB_INT));
        
        //((ComboBox)gui.getGUINode(csg_site_number_combo)).getItems().clear();
        JsonArray numList = programData.getJsonArray(JSON_PROGRAM_NUM_LIST);
        for (int i =0; i<numList.size(); i++) {
            ((ComboBox)gui.getGUINode(csg_site_number_combo)).getItems().add(numList.get(i));
        }
        ((ComboBox)gui.getGUINode(csg_site_number_combo)).getSelectionModel().select(programData.getInt(JSON_PROGRAM_NUM_INT));
        
        ((ComboBox)gui.getGUINode(csg_site_semester_combo)).getSelectionModel().select(programData.getInt(JSON_PROGRAM_SEM_INT));
        
        ((ComboBox)gui.getGUINode(csg_site_year_combo)).getSelectionModel().select(programData.getInt(JSON_PROGRAM_YEAR_INT));

        ((TextField)gui.getGUINode(csg_site_title_textfield)).setText(site.getString(JSON_SITE_TITLE));
        ((ComboBox)gui.getGUINode(csg_site_css_combo)).getSelectionModel().select(programData.getString(JSON_PROGRAM_CSS));

        //pages section
        JsonArray pages = site.getJsonArray(JSON_SITE_PAGES);
        for (int i = 0; i< pages.size(); i++) {
            JsonObject checkbox = pages.getJsonObject(i);
            String name = checkbox.getString(JSON_SITE_PAGES_NAME);
            if (name.equals(JSON_SITE_HOME)) {
                CheckBox c = (CheckBox)gui.getGUINode(csg_site_home_checkbox);
                c.selectedProperty().set(true);
            }
           if (name.equals(JSON_SITE_SYLLABUS)) {
                CheckBox c = (CheckBox)gui.getGUINode(csg_site_syllabus_checkbox);
                c.selectedProperty().set(true);
            }
            if (name.equals(JSON_SITE_SCHEDULE)) {
                CheckBox c = (CheckBox)gui.getGUINode(csg_site_schedule_checkbox);
                c.selectedProperty().set(true);
            }
            if (name.equals(JSON_SITE_HWS)) {
                CheckBox c = (CheckBox)gui.getGUINode(csg_site_hws_checkbox);
                c.selectedProperty().set(true);
            }
        }
        
        //style tab
        Button fav =  (Button)gui.getGUINode(csg_site_favicon_image);
        JsonObject logos = site.getJsonObject(JSON_SITE_LOGOS);
        CourseSiteGeneratorWorkspace work = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        
        if (!logos.getString(JSON_SITE_FAV).isEmpty()) {
            dataManager.openImage(logos.getString(JSON_SITE_FAV), fav);
            work.setFavicon(logos.getString(JSON_SITE_FAV));
        }
        else {
            AppNodesBuilder build = gui.getNodesBuilder();
            build.changeIcon(fav, csg_site_favicon_image, "_ICON", 0, 0);
        }
        Button nav =  (Button)gui.getGUINode(csg_site_navbar_image);
        if (!logos.getString(JSON_SITE_NAV).isEmpty()) {
            dataManager.openImage(logos.getString(JSON_SITE_NAV), nav);
            work.setFavicon(logos.getString(JSON_SITE_NAV));
        }
        else {
            AppNodesBuilder build = gui.getNodesBuilder();
            build.changeIcon(nav, csg_site_navbar_image, "_ICON", 0, 0);
        }
        Button left =  (Button)gui.getGUINode(csg_site_leftFooter_image);
        if (!logos.getString(JSON_SITE_LEFT).toString().isEmpty()) {
             dataManager.openImage(logos.getString(JSON_SITE_LEFT), left);
             work.setFavicon(logos.getString(JSON_SITE_LEFT));
        }
        else {
            AppNodesBuilder build = gui.getNodesBuilder();
            build.changeIcon(left, csg_site_leftFooter_image, "_ICON", 0, 0);
        }
        Button right =  (Button)gui.getGUINode(csg_site_rightFooter_image);
        if (!logos.getString(JSON_SITE_RIGHT).toString().isEmpty()) {
           dataManager.openImage(logos.getString(JSON_SITE_RIGHT), right);
           work.setFavicon(logos.getString(JSON_SITE_RIGHT));//
        }
        else {
            AppNodesBuilder build = gui.getNodesBuilder();
            build.changeIcon(right, csg_site_rightFooter_image, "_ICON", 0, 0);
        }
        
        //instructor box
        JsonObject instructor = site.getJsonObject(JSON_SITE_INSTRUCTOR);
        ((TextField)gui.getGUINode(csg_site_name_textfield)).setText(instructor.getString(JSON_SITE_NAME));
        ((TextField)gui.getGUINode(csg_site_room_textfield)).setText(instructor.getString(JSON_SITE_ROOM));
        ((TextField)gui.getGUINode(csg_site_homepage_textfield)).setText(instructor.getString(JSON_SITE_PAGE));
        ((TextField)gui.getGUINode(csg_site_email_textfield)).setText(instructor.getString(JSON_SITE_EMAIL));
        ((TextArea)gui.getGUINode(csg_site_oh_textarea)).setText(instructor.getString(JSON_SITE_HOURS));
        
        //schedule tab
        JsonObject scheduleObj = wholeJson.getJsonObject(JSON_SCHEDULE);
        String mondayMonth = scheduleObj.getString(JSON_SCHEDULE_MONDAY_MONTH);
        String mondayDay = scheduleObj.getString(JSON_SCHEDULE_MONDAY_DAY);
        String fridayMonth = scheduleObj.getString(JSON_SCHEDULE_FRIDAY_MONTH);
        String fridayDay = scheduleObj.getString(JSON_SCHEDULE_FRIDAY_DAY);
        
        //first, set the calenders (for year, be cheap and use year listed on site page)
        String mondayDate = mondayMonth+"/"+mondayDay;
        String fridayDate = fridayMonth+"/"+fridayDay;
        ((DatePicker)gui.getGUINode(csg_schedule_monday_date)).getEditor().setText(mondayDate);
        ((DatePicker)gui.getGUINode(csg_schedule_friday_date)).getEditor().setText(fridayDate);
        
        
        ///second, iterate through each array type and add the respective item types
        JsonArray holidays = scheduleObj.getJsonArray(JSON_SCHEDULE_HOLIDAYS);
        for (int i=0; i<holidays.size(); i++) {
            JsonObject holiday = holidays.getJsonObject(i);
            String type = "Holiday";
            String date = holiday.getString(JSON_SCHEDULE_MONTH)+"-"+holiday.getString(JSON_SCHEDULE_DAY);
            String topic = holiday.getString(JSON_SCHEDULE_TOPIC);
            String title = holiday.getString(JSON_SCHEDULE_TITLE);
            String link = holiday.getString(JSON_SCHEDULE_LINK);
         dataManager.addSched(new ScheduleItem(type,date,title,topic,link));
        }
        JsonArray lectures = scheduleObj.getJsonArray(JSON_SCHEDULE_LECTURES);
        for (int i=0; i<lectures.size(); i++) {
            JsonObject lecture = lectures.getJsonObject(i);
            String type = "Lecture";
            String date = lecture.getString(JSON_SCHEDULE_MONTH)+"-"+lecture.getString(JSON_SCHEDULE_DAY);
            String topic = lecture.getString(JSON_SCHEDULE_TOPIC);
            String title = lecture.getString(JSON_SCHEDULE_TITLE);
            String link = lecture.getString(JSON_SCHEDULE_LINK);
         dataManager.addSched(new ScheduleItem(type,date,title,topic,link));
        }
        
        JsonArray refs = scheduleObj.getJsonArray(JSON_SCHEDULE_REFS);
        for (int i=0; i<refs.size(); i++) {
            JsonObject ref = refs.getJsonObject(i);
            String type = "Reference";
            String date = ref.getString(JSON_SCHEDULE_MONTH)+"-"+ref.getString(JSON_SCHEDULE_DAY);
            String topic = ref.getString(JSON_SCHEDULE_TOPIC);
            String title = ref.getString(JSON_SCHEDULE_TITLE);
            String link = ref.getString(JSON_SCHEDULE_LINK);
         dataManager.addSched(new ScheduleItem(type,date,title,topic,link));
        }
        JsonArray hws = scheduleObj.getJsonArray(JSON_SCHEDULE_HWS);
        for (int i=0; i<hws.size(); i++) {
            JsonObject hw = hws.getJsonObject(i);
            String type = "HW";
            String date = hw.getString(JSON_SCHEDULE_MONTH)+"-"+hw.getString(JSON_SCHEDULE_DAY);
            String topic = hw.getString(JSON_SCHEDULE_TOPIC);
            String title = hw.getString(JSON_SCHEDULE_TITLE);
            String link = hw.getString(JSON_SCHEDULE_LINK);
         dataManager.addSched(new ScheduleItem(type,date,title,topic,link));
        }
        JsonArray recs = scheduleObj.getJsonArray(JSON_SCHEDULE_RECS);
        for (int i=0; i<hws.size(); i++) {
            JsonObject rec = recs.getJsonObject(i);
            String type = "Recitation";
            String date = rec.getString(JSON_SCHEDULE_MONTH)+"-"+rec.getString(JSON_SCHEDULE_DAY);
            String topic = rec.getString(JSON_SCHEDULE_TOPIC);
            String title = rec.getString(JSON_SCHEDULE_TITLE);
            String link = rec.getString(JSON_SCHEDULE_LINK);
         dataManager.addSched(new ScheduleItem(type,date,title,topic,link));
        }             
        
        
        
        
        //Syllabus Stuff
        JsonObject syllabusObj = wholeJson.getJsonObject(JSON_SYLLABUS);
        String description = syllabusObj.getString(JSON_SYLLABUS_DESCRIPTION);
        String topics = syllabusObj.getString(JSON_SYLLABUS_TOPICS);
        String prereqs = syllabusObj.getString(JSON_SYLLABUS_PREQ);
        String textbooks = syllabusObj.getString(JSON_SYLLABUS_TEXTBOOKS);
        String outcomes = syllabusObj.getString(JSON_SYLLABUS_OUTCOMES);
        String graded = syllabusObj.getString(JSON_SYLLABUS_GRADED);
        String note = syllabusObj.getString(JSON_SYLLABUS_NOTE);
        String dishonesty = syllabusObj.getString(JSON_SYLLABUS_DISHONESTY);
        String assistance = syllabusObj.getString(JSON_SYLLABUS_ASSISTANCE);
        
        HashMap<CourseSiteGeneratorData.syllabusItems, StringProperty> syllabus = dataManager.getSyllabus();
        dataManager.editSyllabus(Description, description);
        ((TextArea)gui.getGUINode(csg_syllabus_description_textarea)).setText(description);
        dataManager.editSyllabus(Topics, topics);
        ((TextArea)gui.getGUINode(csg_syllabus_topics_textarea)).setText(topics);
        dataManager.editSyllabus(Preqs, prereqs);
        ((TextArea)gui.getGUINode(csg_syllabus_prereqs_textarea)).setText(prereqs);
        dataManager.editSyllabus(Textbooks, textbooks);
        ((TextArea)gui.getGUINode(csg_syllabus_textbooks_textarea)).setText(textbooks);
        dataManager.editSyllabus(Outcomes, outcomes);
        ((TextArea)gui.getGUINode(csg_syllabus_outcomes_textarea)).setText(outcomes);
        dataManager.editSyllabus(GradedComponents,graded);
        ((TextArea)gui.getGUINode(csg_syllabus_gradedComponents_textarea)).setText(graded);
        dataManager.editSyllabus(GradingNote, note);
        ((TextArea)gui.getGUINode(csg_syllabus_gradingNote_textarea)).setText(note);
        dataManager.editSyllabus(AcademicDishonesty, dishonesty);
        ((TextArea)gui.getGUINode(csg_syllabus_academicDishonesty_textarea)).setText(dishonesty);
        dataManager.editSyllabus(SpecialAssistance,assistance);
        ((TextArea)gui.getGUINode(csg_syllabus_specialAssistance_textarea)).setText(assistance);
        app.getTPS().clearAllTransactions();
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData)data;
        AppGUIModule gui = app.getGUIModule();

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder gArrayBuilder = Json.createArrayBuilder();
	Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        while (tasIterator.hasNext()) {
            TeachingAssistantPrototype ta = tasIterator.next();
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
	   if(ta.getType()=="undergraduate") taArrayBuilder.add(taJson);
           else gArrayBuilder.add(taJson);
	}
        
	JsonArray undergradTAsArray = taArrayBuilder.build();
        JsonArray gradTAArray = gArrayBuilder.build();
        
        	// NOW BUILD THE Timeslot JSON OBJCTS TO SAVE
	JsonArrayBuilder csgArrayBuilder = Json.createArrayBuilder();
	Iterator<TimeSlot> csgIterator = dataManager.CourseSiteGeneratorIterator();
        String days [] = {"monday","tuesday","wednesday","thursday","friday"};
        while (csgIterator.hasNext()) {
            TimeSlot t = csgIterator.next();
            String time = t.getStartTime();
            for (int i = 0; i<days.length; i++) {
                ArrayList<String> tas = t.getAllTANames(days[i]);
                for (int j = 0; j<tas.size(); j++) {
                    JsonObject tsJson = Json.createObjectBuilder()
                            .add(JSON_NAME, tas.get(j))
                            .add(JSON_DAY_OF_WEEK, days[i])
                            .add(JSON_START_TIME, time)
                            .build();
                    csgArrayBuilder.add(tsJson);
                }   
            }
        }
            JsonArray tsArray = csgArrayBuilder.build();
	

        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAArray)
                .add(JSON_OFFICE_HOURS, tsArray)
		.build();
	
	//Meeting Time Object
        JsonArrayBuilder lectureArrayBuilder = Json.createArrayBuilder();
        Iterator<MeetingTime> lectures = dataManager.LectureIterator();
        while(lectures.hasNext()) {
            MeetingTime time = lectures.next();
            JsonObject lecObj = Json.createObjectBuilder()
              .add(JSON_MEETING_SECTION, time.getSection())
              .add(JSON_MEETING_DAYS, time.getDays())
              .add(JSON_MEETING_TIME, time.getTime())
              .add(JSON_MEETING_LOCATION, time.getRoom())
              .build();
            lectureArrayBuilder.add(lecObj);
        }
	 JsonArray lectureArray = lectureArrayBuilder.build();
        
        JsonArrayBuilder recArrayBuilder = Json.createArrayBuilder();
        Iterator<MeetingTime> recs = dataManager.RecIterator();
        while(recs.hasNext()) {
            MeetingTime time = recs.next();
            JsonObject lecObj = Json.createObjectBuilder()
              .add(JSON_MEETING_SECTION, time.getSection())
              .add(JSON_MEETING_DAYSTIME, time.getDaysAndTime())
              .add(JSON_MEETING_LOCATION, time.getRoom())
              .add(JSON_MEETING_TA_ONE, time.gettaOne())
              .add(JSON_MEETING_TA_TWO, time.getTaTwo())
              .build();
            recArrayBuilder.add(lecObj);
        }
	 JsonArray recArray = recArrayBuilder.build();
         
        JsonArrayBuilder labArrayBuilder = Json.createArrayBuilder();
        Iterator<MeetingTime> labs = dataManager.LabsIterator();
        while(labs.hasNext()) {
            MeetingTime time = labs.next();
            JsonObject lecObj = Json.createObjectBuilder()
              .add(JSON_MEETING_SECTION, time.getSection())
              .add(JSON_MEETING_DAYSTIME, time.getDaysAndTime())
              .add(JSON_MEETING_LOCATION, time.getRoom())
              .add(JSON_MEETING_TA_ONE, time.gettaOne())
              .add(JSON_MEETING_TA_TWO, time.getTaTwo())
              .build();
            labArrayBuilder.add(lecObj);
        }
	 JsonArray labArray = labArrayBuilder.build();
	
        JsonObject meetingTimeObj = Json.createObjectBuilder()
		.add(JSON_MEETING_LECS, lectureArray)
                .add(JSON_MEETING_RECS, recArray)
                .add(JSON_MEETING_LABS, labArray)
		.build();
        
        //Site tab
        String sub = "";
        String sem = "";
        String num = "";
        String year = "";
        String title = "";
        

        
        if (((ComboBox)gui.getGUINode(csg_site_subject_combo)).getSelectionModel().getSelectedItem()!=null) 
            sub = ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getSelectionModel().getSelectedItem().toString();
        if (((ComboBox)gui.getGUINode(csg_site_number_combo)).getSelectionModel().getSelectedItem()!=null)
            num = ((ComboBox)gui.getGUINode(csg_site_number_combo)).getSelectionModel().getSelectedItem().toString();
        if (((ComboBox)gui.getGUINode(csg_site_semester_combo)).getSelectionModel().getSelectedItem()!=null)
            sem = ((ComboBox)gui.getGUINode(csg_site_semester_combo)).getSelectionModel().getSelectedItem().toString();
        if (((ComboBox)gui.getGUINode(csg_site_year_combo)).getSelectionModel().getSelectedItem()!=null)
            year = ((ComboBox)gui.getGUINode(csg_site_year_combo)).getSelectionModel().getSelectedItem().toString();
        title = ((TextField)gui.getGUINode(csg_site_title_textfield)).getText();

        
        
       ArrayList<JsonObject> checkboxes = new ArrayList<JsonObject>();
       if ( ((CheckBox)gui.getGUINode(csg_site_home_checkbox)).isSelected()) {
        JsonObject homePage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_HOME)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_HOME_LINK)
                .build();
        checkboxes.add(homePage);
       }
        if ( ((CheckBox)gui.getGUINode(csg_site_syllabus_checkbox)).isSelected()) {
        JsonObject syllabusPage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_SYLLABUS)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_SYLLABUS_LINK)
                .build();
        checkboxes.add(syllabusPage);
        }
         if ( ((CheckBox)gui.getGUINode(csg_site_schedule_checkbox)).isSelected()) {
         JsonObject schedulePage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_SCHEDULE)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_SCHEDULE_LINK)
                .build();
         checkboxes.add(schedulePage);
         }
        if ( ((CheckBox)gui.getGUINode(csg_site_hws_checkbox)).isSelected()) {
        JsonObject hwsPage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_HWS)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_HWS_LINK)
                .build();
        checkboxes.add(hwsPage);
          }
        JsonArrayBuilder pagesArray = Json.createArrayBuilder();
        for (int i=0; i<checkboxes.size(); i++) {
            pagesArray.add(checkboxes.get(i));
        }
        JsonArray pages = pagesArray.build();
        CourseSiteGeneratorWorkspace work = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        String favicon = "";
        String navbar = "";
        String left = "";
        String right = "";
        
        if(work!=null) {
            favicon = work.getFavicon();
            navbar = work.getNavBar();
            left = work.getLeft();
            right = work.getRight();
        }
        
        JsonObject logos = Json.createObjectBuilder()
                .add(JSON_SITE_FAV,favicon)
                .add(JSON_SITE_NAV,navbar)
                .add(JSON_SITE_LEFT, left)
                .add(JSON_SITE_RIGHT, right)
                .build();
        
        
         String name = ((TextField)gui.getGUINode(csg_site_name_textfield)).getText();
         String link = ((TextField)gui.getGUINode(csg_site_homepage_textfield)).getText();
         String room = ((TextField)gui.getGUINode(csg_site_room_textfield)).getText();
         String email = ((TextField)gui.getGUINode(csg_site_email_textfield)).getText();
         String hours = ((TextArea)gui.getGUINode(csg_site_oh_textarea)).getText();
        
        JsonObject instructor = Json.createObjectBuilder()
                .add(JSON_SITE_NAME,name)
                .add(JSON_SITE_PAGE,link)
                .add(JSON_SITE_ROOM, room)
                .add(JSON_SITE_EMAIL, email)
                .add(JSON_SITE_PHOTO, "")
                .add(JSON_SITE_HOURS, hours)

                .build();
        
                           
        
        
        JsonObject siteObj = Json.createObjectBuilder()
               .add(JSON_SITE_SUB, sub)
               .add(JSON_SITE_NUM, num)
               .add(JSON_SITE_SEM, sem)
               .add(JSON_SITE_YEAR, year)
               .add(JSON_SITE_TITLE, title)
               .add(JSON_SITE_INSTRUCTOR, instructor)
               .add(JSON_SITE_LOGOS, logos)
               .add(JSON_SITE_PAGES,pages)
               .build();
                
        
        
        
        
        
        //Syllabus Stuff
        HashMap<CourseSiteGeneratorData.syllabusItems, StringProperty> hash = dataManager.getSyllabus();
        JsonObject syllabusObj = Json.createObjectBuilder()
                .add(JSON_SYLLABUS_DESCRIPTION, hash.get(Description).getValue())
                .add(JSON_SYLLABUS_TOPICS, hash.get(Topics).getValue())
                .add(JSON_SYLLABUS_PREQ, hash.get(Preqs).getValue())
                .add(JSON_SYLLABUS_TEXTBOOKS, hash.get(Textbooks).getValue())
                .add(JSON_SYLLABUS_OUTCOMES, hash.get(Outcomes).getValue())
                .add(JSON_SYLLABUS_GRADED, hash.get(GradedComponents).getValue())
                .add(JSON_SYLLABUS_NOTE, hash.get(GradingNote).getValue())
                .add(JSON_SYLLABUS_DISHONESTY,hash.get(AcademicDishonesty).getValue())
                .add(JSON_SYLLABUS_ASSISTANCE, hash.get(SpecialAssistance).getValue())
                .build();
        
        ObservableList<String> subList = ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getItems();
        JsonArrayBuilder subBuilder = Json.createArrayBuilder();
        for (int i = 0; i<subList.size(); i++) {
            if(subList.get(i)!=null)
            subBuilder.add(subList.get(i));
        }
        JsonArray subArray = subBuilder.build();
        
        ComboBox b = (ComboBox)gui.getGUINode(csg_site_number_combo);
        ObservableList<String> numList = ((ComboBox)gui.getGUINode(csg_site_number_combo)).getItems();
        JsonArrayBuilder numBuilder = Json.createArrayBuilder();
        for (int i = 0; i<numList.size(); i++) {
           if(numList.get(i)!=null)
            numBuilder.add(numList.get(i));
        }
        JsonArray numArray = subBuilder.build();
        
        ObservableList<String> yearList = ((ComboBox)gui.getGUINode(csg_site_year_combo)).getItems();
        JsonArrayBuilder yearBuilder = Json.createArrayBuilder();
        for (int i = 0; i<yearList.size(); i++) {
            if (yearList.get(i)!=null)
            yearBuilder.add(yearList.get(i));
        }
        JsonArray yearArray = yearBuilder.build();
        
        ObservableList<String> semList = ((ComboBox)gui.getGUINode(csg_site_semester_combo)).getItems();
        JsonArrayBuilder semBuilder = Json.createArrayBuilder();
        for (int i = 0; i<semList.size(); i++) {
            if (semList.get(i)!=null)
            semBuilder.add(semList.get(i));
        }
        JsonArray semArray = semBuilder.build();
        String cssString = "";
        if (((ComboBox)gui.getGUINode(csg_site_css_combo)).valueProperty().getValue()!=null) cssString = ((ComboBox)gui.getGUINode(csg_site_css_combo)).valueProperty().getValue().toString();
        
        JsonObject programDataObj = Json.createObjectBuilder()
                .add(JSON_PROGRAM_SUB_INT,((ComboBox)gui.getGUINode(csg_site_subject_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_SUB_LIST,subArray)
                .add(JSON_PROGRAM_NUM_INT,((ComboBox)gui.getGUINode(csg_site_number_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_NUM_LIST,numArray)
                .add(JSON_PROGRAM_YEAR_INT,((ComboBox)gui.getGUINode(csg_site_year_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_YEAR_LIST,yearArray)
                .add(JSON_PROGRAM_SEM_INT,((ComboBox)gui.getGUINode(csg_site_semester_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_SEM_LIST,semArray)
                .add(JSON_PROGRAM_CSS,cssString)
                .build();
        
        //schedule tab
        JsonArrayBuilder lecArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder hwArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder holidayArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder refArrayBuilder = Json.createArrayBuilder();
        recArrayBuilder = Json.createArrayBuilder();
        Iterator<ScheduleItem> items = dataManager.itemIterator();
        while(items.hasNext()) {
            ScheduleItem item = items.next();
            JsonObject itemObj;
            if(item.getType().equals("Lecture")) {
             itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
             lecArrayBuilder.add(itemObj);
            }
            else if (item.getType().equals("HW")) {
              itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              hwArrayBuilder.add(itemObj);
            }
            else if (item.getType().equals("Holiday")) {
              itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              holidayArrayBuilder.add(itemObj);
            }
            else if (item.getType().equals("Recitation")) {
             itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              recArrayBuilder.add(itemObj);
                
            }
            else {
              itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              refArrayBuilder.add(itemObj);
            }
        }
	 JsonArray lecs = lecArrayBuilder.build();
         JsonArray hws = hwArrayBuilder.build();
         JsonArray holidays = holidayArrayBuilder.build();
         JsonArray refs = refArrayBuilder.build();
         JsonArray reci = recArrayBuilder.build();
        
         String mondayMonth = "";
         String mondayDay = "";
         String fridayMonth = "";
         String fridayDay = "";
         DatePicker starter = (DatePicker)gui.getGUINode(csg_schedule_monday_date);
         if (starter.getEditor().getText().length()>=3) {
         mondayMonth = ""+starter.getEditor().getText().substring(0,2);
         mondayDay = ""+starter.getEditor().getText().substring(3);
         if (mondayDay.length()>2) mondayDay = mondayDay.substring(0,2);
         if(mondayDay.contains("/")) mondayDay=mondayDay.substring(0,1);
         }

         
         
         DatePicker ender = (DatePicker)gui.getGUINode(csg_schedule_friday_date);
         if (ender.getEditor().getText().length()>=3) {
         fridayMonth = ""+ender.getEditor().getText().substring(0,2);
         fridayDay = ""+ender.getEditor().getText().substring(3);
         if (fridayDay.length()>2) fridayDay = fridayDay.substring(0,2);
         if(fridayDay.contains("/")) fridayDay=fridayDay.substring(0,1);
         }
        

        
         JsonObject schedule = Json.createObjectBuilder()
                 .add(JSON_SCHEDULE_MONDAY_MONTH, mondayMonth)
                 .add(JSON_SCHEDULE_MONDAY_DAY,mondayDay)
                 .add(JSON_SCHEDULE_FRIDAY_MONTH, fridayMonth)
                 .add(JSON_SCHEDULE_FRIDAY_DAY,fridayDay)
                 .add(JSON_SCHEDULE_LECTURES, lecs)
                 .add(JSON_SCHEDULE_HWS,hws)
                 .add(JSON_SCHEDULE_HOLIDAYS,holidays)
                 .add(JSON_SCHEDULE_REFS, refs)
                 .add(JSON_SCHEDULE_RECS,reci)
                 .build();
                 
        
        //buildWholeObject
        JsonObject wholeObject = Json.createObjectBuilder()
                .add(JSON_OFFICE_HOURS,dataManagerJSO)
                .add(JSON_MEETING,meetingTimeObj)
                .add(JSON_SYLLABUS, syllabusObj)
                .add(JSON_SCHEDULE,schedule)
                .add(JSON_SITE, siteObj)
                .add(JSON_PROGRAM,programDataObj)
                .build();
	
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        StringWriter sw = new StringWriter();
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(wholeObject);
	jsonWriter.close();

	// INIT THE WRITER
	
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        AppGUIModule gui = app.getGUIModule();
        
        String sub = "";
        String sem = "";
        String num = "";
        String year = "";
        String title = "";
        

        
        if (((ComboBox)gui.getGUINode(csg_site_subject_combo)).getSelectionModel().getSelectedItem()!=null) 
            sub = ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getSelectionModel().getSelectedItem().toString();
        if (((ComboBox)gui.getGUINode(csg_site_number_combo)).getSelectionModel().getSelectedItem()!=null)
            num = ((ComboBox)gui.getGUINode(csg_site_number_combo)).getSelectionModel().getSelectedItem().toString();
        if (((ComboBox)gui.getGUINode(csg_site_semester_combo)).getSelectionModel().getSelectedItem()!=null)
            sem = ((ComboBox)gui.getGUINode(csg_site_semester_combo)).getSelectionModel().getSelectedItem().toString();
        if (((ComboBox)gui.getGUINode(csg_site_year_combo)).getSelectionModel().getSelectedItem()!=null)
            year = ((ComboBox)gui.getGUINode(csg_site_year_combo)).getSelectionModel().getSelectedItem().toString();
        title = ((TextField)gui.getGUINode(csg_site_title_textfield)).getText();
        
        
        String folderPath = "export\\"+sub+"_"+num+"_"+sem+"_"+year;
        String exportDir = ((Label)gui.getGUINode(csg_site_exportVal_label)).getText();
        String exportWarn = ((Label)gui.getGUINode(csg_site_export_dir_warning)).getText();
        if (exportDir.equals(exportWarn)) return;
        //String folderPath = "export\\"+filePath.substring(0,filePath.indexOf(".json")); //removes .json from folder name 
        System.out.println(folderPath);
        
        //make project folder
        if(!new File(folderPath).exists()) {
            new File(folderPath).mkdir();
        }
        //copies all src code contents into folder
        File src = new File("srcCode");
        File dest = new File(folderPath); 
        copyFolder(src,dest);
        src = new File("images");
        copyFolder(src,dest);

        
        
        	// GET THE DATA
	CourseSiteGeneratorData dataManager = (CourseSiteGeneratorData)data;
        

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
	JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder gArrayBuilder = Json.createArrayBuilder();
	Iterator<TeachingAssistantPrototype> tasIterator = dataManager.teachingAssistantsIterator();
        while (tasIterator.hasNext()) {
            TeachingAssistantPrototype ta = tasIterator.next();
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
                    .add(JSON_EMAIL, ta.getEmail()).build();
	   if(ta.getType()=="undergraduate") taArrayBuilder.add(taJson);
           else gArrayBuilder.add(taJson);
	}
        
	JsonArray undergradTAsArray = taArrayBuilder.build();
        JsonArray gradTAArray = gArrayBuilder.build();
        
        	// NOW BUILD THE Timeslot JSON OBJCTS TO SAVE
	JsonArrayBuilder csgArrayBuilder = Json.createArrayBuilder();
	Iterator<TimeSlot> csgIterator = dataManager.CourseSiteGeneratorIterator();
        String days [] = {"MONDAY","TUESDAY","WEDNESDAY","THURSDAY","FRIDAY"};
        while (csgIterator.hasNext()) {
            TimeSlot t = csgIterator.next();
            String time = t.getStartTime();
            time=time.replace(":", "_");
            for (int i = 0; i<days.length; i++) {
                ArrayList<String> tas = t.getAllTANames(days[i].toLowerCase());
                for (int j = 0; j<tas.size(); j++) {
                    JsonObject tsJson = Json.createObjectBuilder()
                            .add(JSON_NAME, tas.get(j))
                            .add(JSON_DAY_OF_WEEK, days[i])
                            .add(JSON_START_TIME, time)
                            .build();
                    csgArrayBuilder.add(tsJson);
                }   
            }
        }
            JsonArray tsArray = csgArrayBuilder.build();
	
       
         String name = ((TextField)gui.getGUINode(csg_site_name_textfield)).getText();
         String link = ((TextField)gui.getGUINode(csg_site_homepage_textfield)).getText();
         String room = ((TextField)gui.getGUINode(csg_site_room_textfield)).getText();
         String email = ((TextField)gui.getGUINode(csg_site_email_textfield)).getText();
         String hours = ((TextArea)gui.getGUINode(csg_site_oh_textarea)).getText();
        
         
         //NOTE: the instructor object is used in both officehours json and pages json
        JsonObject instructor = Json.createObjectBuilder()
                .add(JSON_SITE_NAME,name)
                .add(JSON_SITE_PAGE,link)
                .add(JSON_SITE_ROOM, room)
                .add(JSON_SITE_EMAIL, email)
                .add(JSON_SITE_PHOTO, "")
                .add(JSON_SITE_HOURS, hours)

                .build();
            
        
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAArray)
                .add(JSON_OFFICE_HOURS, tsArray)
                .add(JSON_SITE_INSTRUCTOR,instructor)
		.build();
	
	//Meeting Time Object
        JsonArrayBuilder lectureArrayBuilder = Json.createArrayBuilder();
        Iterator<MeetingTime> lectures = dataManager.LectureIterator();
        while(lectures.hasNext()) {
            MeetingTime time = lectures.next();
            JsonObject lecObj = Json.createObjectBuilder()
              .add(JSON_MEETING_SECTION, time.getSection())
              .add(JSON_MEETING_DAYS, time.getDays())
              .add(JSON_MEETING_TIME, time.getTime())
              .add(JSON_MEETING_LOCATION, time.getRoom())
              .build();
            lectureArrayBuilder.add(lecObj);
        }
	 JsonArray lectureArray = lectureArrayBuilder.build();
        
        JsonArrayBuilder recArrayBuilder = Json.createArrayBuilder();
        Iterator<MeetingTime> recs = dataManager.RecIterator();
        while(recs.hasNext()) {
            MeetingTime time = recs.next();
            JsonObject lecObj = Json.createObjectBuilder()
              .add(JSON_MEETING_SECTION, time.getSection())
              .add(JSON_MEETING_DAYSTIME, time.getDaysAndTime())
              .add(JSON_MEETING_LOCATION, time.getRoom())
              .add(JSON_MEETING_TA_ONE, time.gettaOne())
              .add(JSON_MEETING_TA_TWO, time.getTaTwo())
              .build();
            recArrayBuilder.add(lecObj);
        }
	 JsonArray recArray = recArrayBuilder.build();
         
        JsonArrayBuilder labArrayBuilder = Json.createArrayBuilder();
        Iterator<MeetingTime> labs = dataManager.LabsIterator();
        while(labs.hasNext()) {
            MeetingTime time = labs.next();
            JsonObject lecObj = Json.createObjectBuilder()
              .add(JSON_MEETING_SECTION, time.getSection())
              .add(JSON_MEETING_DAYSTIME, time.getDaysAndTime())
              .add(JSON_MEETING_LOCATION, time.getRoom())
              .add(JSON_MEETING_TA_ONE, time.gettaOne())
              .add(JSON_MEETING_TA_TWO, time.getTaTwo())
              .build();
            labArrayBuilder.add(lecObj);
        }
	 JsonArray labArray = labArrayBuilder.build();
	
        JsonObject meetingTimeObj = Json.createObjectBuilder()
		.add(JSON_MEETING_LECS, lectureArray)
                .add(JSON_MEETING_RECS, recArray)
                .add(JSON_MEETING_LABS, labArray)
		.build();
        
        //Site tab


        
        
       ArrayList<JsonObject> checkboxes = new ArrayList<JsonObject>();
       if ( ((CheckBox)gui.getGUINode(csg_site_home_checkbox)).isSelected()) {
        JsonObject homePage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_HOME)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_HOME_LINK)
                .build();
        checkboxes.add(homePage);
       }
        if ( ((CheckBox)gui.getGUINode(csg_site_syllabus_checkbox)).isSelected()) {
        JsonObject syllabusPage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_SYLLABUS)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_SYLLABUS_LINK)
                .build();
        checkboxes.add(syllabusPage);
        }
         if ( ((CheckBox)gui.getGUINode(csg_site_schedule_checkbox)).isSelected()) {
         JsonObject schedulePage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_SCHEDULE)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_SCHEDULE_LINK)
                .build();
         checkboxes.add(schedulePage);
         }
        if ( ((CheckBox)gui.getGUINode(csg_site_hws_checkbox)).isSelected()) {
        JsonObject hwsPage = Json.createObjectBuilder()
                .add(JSON_SITE_PAGES_NAME, JSON_SITE_HWS)
                .add(JSON_SITE_PAGES_LINK, JSON_SITE_HWS_LINK)
                .build();
        checkboxes.add(hwsPage);
          }
        JsonArrayBuilder pagesArray = Json.createArrayBuilder();
        for (int i=0; i<checkboxes.size(); i++) {
            pagesArray.add(checkboxes.get(i));
        }
        JsonArray pages = pagesArray.build();
        CourseSiteGeneratorWorkspace work = (CourseSiteGeneratorWorkspace)app.getWorkspaceComponent();
        String favicon = "";
        String navbar = "";
        String left = "";
        String right = "";
        
        if(work!=null) {
            favicon = "./"+work.getFavicon();
            navbar = "./"+work.getNavBar();
            left = "./"+work.getLeft();
            right = "./"+work.getRight();
        }
        JsonObject faviconObj = Json.createObjectBuilder()
                .add("href",favicon)
                .build();
        JsonObject navbarObj = Json.createObjectBuilder()
                .add("href"," ")
                .add("src",navbar)
                .build();
        JsonObject leftObj = Json.createObjectBuilder()
                .add("href"," ")
                .add("src",left)
                .build();
        JsonObject rightObj = Json.createObjectBuilder()
                .add("href"," ")
                .add("src",right)
                .build();
        
        JsonObject logos = Json.createObjectBuilder()
                .add(JSON_SITE_FAV,faviconObj)
                .add(JSON_SITE_NAV,navbarObj)
                .add(JSON_SITE_LEFT, leftObj)
                .add(JSON_SITE_RIGHT, rightObj)
                .build();
        
 
        
                           
        
        
        JsonObject siteObj = Json.createObjectBuilder()
               .add(JSON_SITE_SUB, sub)
               .add(JSON_SITE_NUM, num)
               .add(JSON_SITE_SEM, sem)
               .add(JSON_SITE_YEAR, year)
               .add(JSON_SITE_TITLE, title)
               .add(JSON_SITE_INSTRUCTOR, instructor)
               .add(JSON_SITE_LOGOS, logos)
               .add(JSON_SITE_PAGES,pages)
               .build();
                
        
        
        
        
        
        //Syllabus Stuff
        HashMap<CourseSiteGeneratorData.syllabusItems, StringProperty> hash = dataManager.getSyllabus();
        JsonObject syllabusObj = Json.createObjectBuilder()
                .add(JSON_SYLLABUS_DESCRIPTION, hash.get(Description).getValue())
                .add(JSON_SYLLABUS_TOPICS, hash.get(Topics).getValue())
                .add(JSON_SYLLABUS_PREQ, hash.get(Preqs).getValue())
                .add(JSON_SYLLABUS_TEXTBOOKS, hash.get(Textbooks).getValue())
                .add(JSON_SYLLABUS_OUTCOMES, hash.get(Outcomes).getValue())
                .add(JSON_SYLLABUS_GRADED, hash.get(GradedComponents).getValue())
                .add(JSON_SYLLABUS_NOTE, hash.get(GradingNote).getValue())
                .add(JSON_SYLLABUS_DISHONESTY,hash.get(AcademicDishonesty).getValue())
                .add(JSON_SYLLABUS_ASSISTANCE, hash.get(SpecialAssistance).getValue())
                .build();
        
        ObservableList<String> subList = ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getItems();
        JsonArrayBuilder subBuilder = Json.createArrayBuilder();
        for (int i = 0; i<subList.size(); i++) {
            subBuilder.add(subList.get(i));
        }
        JsonArray subArray = subBuilder.build();
        
        ComboBox b = (ComboBox)gui.getGUINode(csg_site_number_combo);
        ObservableList<String> numList = ((ComboBox)gui.getGUINode(csg_site_number_combo)).getItems();
        JsonArrayBuilder numBuilder = Json.createArrayBuilder();
        for (int i = 0; i<numList.size(); i++) {
           if(numList.get(i)!=null)
            numBuilder.add(numList.get(i));
        }
        JsonArray numArray = subBuilder.build();
        
        ObservableList<String> yearList = ((ComboBox)gui.getGUINode(csg_site_year_combo)).getItems();
        JsonArrayBuilder yearBuilder = Json.createArrayBuilder();
        for (int i = 0; i<yearList.size(); i++) {
            if(yearList.get(i)!=null)
            yearBuilder.add(yearList.get(i));
        }
        JsonArray yearArray = yearBuilder.build();
        
        ObservableList<String> semList = ((ComboBox)gui.getGUINode(csg_site_semester_combo)).getItems();
        JsonArrayBuilder semBuilder = Json.createArrayBuilder();
        for (int i = 0; i<semList.size(); i++) {
            if(semList.get(i)!=null)
            semBuilder.add(semList.get(i));
        }
        JsonArray semArray = semBuilder.build();
        String cssString = "";
        if (((ComboBox)gui.getGUINode(csg_site_css_combo)).valueProperty().getValue()!=null) cssString = ((ComboBox)gui.getGUINode(csg_site_css_combo)).valueProperty().getValue().toString();
        
        JsonObject programDataObj = Json.createObjectBuilder()
                .add(JSON_PROGRAM_SUB_INT,((ComboBox)gui.getGUINode(csg_site_subject_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_SUB_LIST,subArray)
                .add(JSON_PROGRAM_NUM_INT,((ComboBox)gui.getGUINode(csg_site_number_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_NUM_LIST,numArray)
                .add(JSON_PROGRAM_YEAR_INT,((ComboBox)gui.getGUINode(csg_site_year_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_YEAR_LIST,yearArray)
                .add(JSON_PROGRAM_SEM_INT,((ComboBox)gui.getGUINode(csg_site_semester_combo)).getSelectionModel().getSelectedIndex())
                .add(JSON_PROGRAM_SEM_LIST,semArray)
                .add(JSON_PROGRAM_CSS,cssString)
                .build();
        
         //schedule tab
        JsonArrayBuilder lecArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder hwArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder holidayArrayBuilder = Json.createArrayBuilder();
        JsonArrayBuilder refArrayBuilder = Json.createArrayBuilder();
        recArrayBuilder = Json.createArrayBuilder();
        Iterator<ScheduleItem> items = dataManager.itemIterator();
        while(items.hasNext()) {
            ScheduleItem item = items.next();
            JsonObject itemObj;
            if(item.getType().equals("Lecture")) {
             itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
             lecArrayBuilder.add(itemObj);
            }
            else if (item.getType().equals("HW")) {
              itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              hwArrayBuilder.add(itemObj);
            }
            else if (item.getType().equals("Holiday")) {
              itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              holidayArrayBuilder.add(itemObj);
            }
            else if (item.getType().equals("Recitation")) {
             itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              recArrayBuilder.add(itemObj);
                
            }
            else {
              itemObj = Json.createObjectBuilder()
              .add(JSON_SCHEDULE_MONTH, item.getDate().substring(0, 2))
              .add(JSON_SCHEDULE_DAY, item.getDate().substring(3))
              .add(JSON_SCHEDULE_TITLE, item.getTitle())
              .add(JSON_SCHEDULE_TOPIC, item.getTopic())
              .add(JSON_SCHEDULE_LINK, item.getLink())
              .build();
              refArrayBuilder.add(itemObj);
            }
        }
	 JsonArray lecs = lecArrayBuilder.build();
         JsonArray hws = hwArrayBuilder.build();
         JsonArray holidays = holidayArrayBuilder.build();
         JsonArray refs = refArrayBuilder.build();
         JsonArray reci = recArrayBuilder.build();
         
         String mondayMonth = "";
         String mondayDay = "";
         String fridayMonth = "";
         String fridayDay = "";
           DatePicker starter = (DatePicker)gui.getGUINode(csg_schedule_monday_date);
         if (starter.getEditor().getText().length()>=3) {
         mondayMonth = ""+starter.getEditor().getText().substring(0,2);
         mondayDay = ""+starter.getEditor().getText().substring(3);
         if (mondayDay.length()>2) mondayDay = mondayDay.substring(0,2);
         if(mondayDay.contains("/")) mondayDay=mondayDay.substring(0,1);
         }

         
         
         DatePicker ender = (DatePicker)gui.getGUINode(csg_schedule_friday_date);
         if (ender.getEditor().getText().length()>=3) {
         fridayMonth = ""+ender.getEditor().getText().substring(0,2);
         fridayDay = ""+ender.getEditor().getText().substring(3);
         if (fridayDay.length()>2) fridayDay = fridayDay.substring(0,2);
         if(fridayDay.contains("/")) fridayDay=fridayDay.substring(0,1);
         
         }
        
         JsonObject schedule = Json.createObjectBuilder()
                 .add(JSON_SCHEDULE_MONDAY_MONTH, mondayMonth)
                 .add(JSON_SCHEDULE_MONDAY_DAY,mondayDay)
                 .add(JSON_SCHEDULE_FRIDAY_MONTH, fridayMonth)
                 .add(JSON_SCHEDULE_FRIDAY_DAY,fridayDay)
                 .add(JSON_SCHEDULE_LECTURES, lecs)
                 .add(JSON_SCHEDULE_HWS,hws)
                 .add(JSON_SCHEDULE_HOLIDAYS,holidays)
                 .add(JSON_SCHEDULE_REFS, refs)
                 .add(JSON_SCHEDULE_RECS,reci)
                 .build();
        
        
        //copy css file
        File cssDest = new File(folderPath+"\\css\\sea_wolf.css");
        File cssSrc = new File(("work\\css\\"+cssString));
        copyFile(cssSrc,cssDest);
        
        
        //buildWholeObject
        JsonObject wholeObject = Json.createObjectBuilder()
                .add(JSON_OFFICE_HOURS,dataManagerJSO)
                .add(JSON_MEETING,meetingTimeObj)
                .add(JSON_SYLLABUS, syllabusObj)
                .add(JSON_SITE, siteObj)
                .add(JSON_PROGRAM,programDataObj)
                .build();
	
        //Make file paths
        String ohFile = folderPath+"\\js\\OfficeHoursData.json";
        String pageFile = folderPath+"\\js\\PageData.json";
        String schedFile = folderPath+"\\js\\ScheduleData.json";
        String sylFile = folderPath+"\\js\\SyllabusData.json";
        String meetFile = folderPath+"\\js\\SectionsData.json";
        
        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        
        //first make pretty printing properties.
        Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
        
        //OH file
        //make string writer and put object in it. 
        StringWriter ohSW = new StringWriter();
        JsonWriterFactory ohWriterFactory = Json.createWriterFactory(properties);
        JsonWriter ohJsonWriter = ohWriterFactory.createWriter(ohSW);
	ohJsonWriter.writeObject(dataManagerJSO);
	ohJsonWriter.close();

	// Init writer and save
	String ohPrettyPrinted = ohSW.toString();
	PrintWriter ohPW = new PrintWriter(ohFile);
	ohPW.write(ohPrettyPrinted);
	ohPW.close();
        
                //SYL file
        //make string writer and put object in it. 
        StringWriter sylSW = new StringWriter();
        JsonWriterFactory sylWriterFactory = Json.createWriterFactory(properties);
        JsonWriter sylJsonWriter = sylWriterFactory.createWriter(sylSW);
	sylJsonWriter.writeObject(syllabusObj);
	sylJsonWriter.close();

	// Init writer and save
	String sylPrettyPrinted = sylSW.toString();
	PrintWriter sylPW = new PrintWriter(sylFile);
	sylPW.write(sylPrettyPrinted);
	sylPW.close();
        
                //Schedule file
        //make string writer and put object in it. 
        StringWriter schedSW = new StringWriter();
        JsonWriterFactory schedWriterFactory = Json.createWriterFactory(properties);
        JsonWriter schedJsonWriter = schedWriterFactory.createWriter(schedSW);
	schedJsonWriter.writeObject(schedule);
	schedJsonWriter.close();

	// Init writer and save
	String schedPrettyPrinted = schedSW.toString();
	PrintWriter schedPW = new PrintWriter(schedFile);
	schedPW.write(schedPrettyPrinted);
	schedPW.close();
        
                //Meeting time file
        //make string writer and put object in it. 
        StringWriter meetSW = new StringWriter();
        JsonWriterFactory meetWriterFactory = Json.createWriterFactory(properties);
        JsonWriter meetJsonWriter = meetWriterFactory.createWriter(meetSW);
	meetJsonWriter.writeObject(meetingTimeObj);
	meetJsonWriter.close();

	// Init writer and save
	String meetPrettyPrinted = meetSW.toString();
	PrintWriter meetPW = new PrintWriter(meetFile);
	meetPW.write(meetPrettyPrinted);
	meetPW.close();
        
                //Page file
        //make string writer and put object in it. 
        StringWriter pageSW = new StringWriter();
        JsonWriterFactory pageWriterFactory = Json.createWriterFactory(properties);
        JsonWriter pageJsonWriter = pageWriterFactory.createWriter(pageSW);
	pageJsonWriter.writeObject(siteObj);
	pageJsonWriter.close();

	// Init writer and save
	String pagePrettyPrinted = pageSW.toString();
	PrintWriter pagePW = new PrintWriter(pageFile);
	pagePW.write(pagePrettyPrinted);
	pagePW.close();
        
        folderPath = folderPath.replace("export\\", "");
        
        showExportDialogue(folderPath);
        
        
        
    }
    public static void copyFolder(File source, File destination)
{
    if (source.isDirectory())
    {
        if (!destination.exists())
        {
            destination.mkdirs();
        }

        String files[] = source.list();

        for (String file : files)
        {
            File srcFile = new File(source, file);
            File destFile = new File(destination, file);

            copyFolder(srcFile, destFile);
        }
    }
    else
    {
        InputStream in = null;
        OutputStream out = null;

        try
        {
            in = new FileInputStream(source);
            out = new FileOutputStream(destination);

            byte[] buffer = new byte[1024];

            int length;
            while ((length = in.read(buffer)) > 0)
            {
                out.write(buffer, 0, length);
            }
        }
        catch (Exception e)
        {
            try
            {
                in.close();
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }

            try
            {
                out.close();
            }
            catch (IOException e1)
            {
                e1.printStackTrace();
            }
        }
    }
}
    public void copyFile(File srcFile, File destination) throws IOException {
        
        try {
        if (destination.exists()) destination.delete();
        Files.copy(srcFile.toPath(), destination.toPath());
        }
        catch(IOException err) {
            System.out.println(err);
        }
    }
    public void showExportDialogue(String filePath) {
        AppWebDialog dialog = new AppWebDialog(app);
        WebView web = dialog.getWebView();
        dialog.showWebPage(filePath);
    }
}