package csg.transactions;

import csg.data.CourseSiteGeneratorData;
import csg.data.ScheduleItem;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jtps.jTPS_Transaction;

/**
 *
 * @author McKillaGorilla
 */
public class Clear_Transaction implements jTPS_Transaction {
    CourseSiteGeneratorData data;
    ObservableList<ScheduleItem> newList = FXCollections.observableArrayList();
    
    public Clear_Transaction(CourseSiteGeneratorData initData) {
        data = initData;
        Iterator<ScheduleItem> list = data.itemIterator();
        while(list.hasNext()) {
            newList.add(list.next());
        }
    }

    @Override
    public void doTransaction() {
        data.clearSchedule();
    }

    @Override
    public void undoTransaction() {
        data.setSchedule(newList);
    }
}
