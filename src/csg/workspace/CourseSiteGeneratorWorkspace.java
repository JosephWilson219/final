package csg.workspace;

import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import djf.ui.AppNodesBuilder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import csg.CourseSiteGeneratorApp;
import static csg.CourseSiteGeneratorPropertyType.*;
import csg.data.CourseSiteGeneratorData;
import static csg.data.CourseSiteGeneratorData.MAX_END_HOUR;
import csg.data.CourseSiteGeneratorData.syllabusItems;
import csg.data.MeetingTime;
import csg.data.TeachingAssistantPrototype;
import csg.data.TimeSlot;
import csg.transactions.ADD_EDIT_TA_Transcation;
import csg.transactions.AddCut_Transaction;
import csg.transactions.AddEdit_Meeting_Transaction;
import csg.transactions.AddEdit_Site_Transaction;
import csg.transactions.AddEdit_Syllabus_Transaction;
import csg.transactions.AddOH_Transaction;
import csg.workspace.controllers.CourseSiteGeneratorController;
import csg.workspace.foolproof.CourseSiteGeneratorFoolproofDesign;
import static csg.workspace.style.CSGStyle.*;
import static djf.AppPropertyType.COPY_BUTTON;
import static djf.AppPropertyType.CUT_BUTTON;
import static djf.modules.AppGUIModule.ENABLED;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;

/**
 *
 * @author McKillaGorilla
 */
public class CourseSiteGeneratorWorkspace extends AppWorkspaceComponent {
    String faviconPath = "";
    String navBarPath = "";
    String leftFooterPath = "";
    String rightFooterPath = "";
    Stage taWindow = new Stage();
    final FileChooser fileChooser = new FileChooser();
    
    
    ObservableList<String> subjects = 
    FXCollections.observableArrayList(
        "CSE",
        "AMS",
        "ISE",
        "PHY",
        "PSY",
        "BIO",
        "CHE",
        "MAT"
    );
    
    ObservableList<String> numbers = 
    FXCollections.observableArrayList(
        "100"
    );
    ObservableList<String> semesters = 
    FXCollections.observableArrayList(
        "Fall",
        "Winter",
        "Spring",
        "Summer"
    );
        ObservableList<String> years = 
    FXCollections.observableArrayList(
        "2017",
        "2018",
        "2019",
        "2020",
        "2021",
        "2022",
        "2023",
        "2024",
        "2025",
        "2026",
        "2027",
        "2028",
        "2029",
        "2030",
        "2031",
        "2032",
        "2033",
        "2034",
        "2035",
        "2036",
        "2037",
        "2038",
        "2039"
    );
    
    
    ObservableList<String> timesStart = 
    FXCollections.observableArrayList(
       
    );
    
    ObservableList<String> timesEnd = 
    FXCollections.observableArrayList(
       
    );
       ObservableList<String> cssFiles = 
    FXCollections.observableArrayList(
    );
           ObservableList<String> syllabusItemPropertyNames = 
    FXCollections.observableArrayList(
            "description",
            "topics",
            "prereqs",
            "outcomes",
            "textbooks",
            "gradedComponents",
            "gradingNote",
            "academicDishonesty",
            "specialAssistance"
    );
           
    ObservableList<String> typeOptions = 
    FXCollections.observableArrayList(
            "Holiday",
            "Lecture",
            "HW"
    );

    
    public CourseSiteGeneratorWorkspace(CourseSiteGeneratorApp app) {
        super(app);
        numbers.clear();
        for (int i = 101; i<600; i++) {
            numbers.add(""+i);
        }
        updateCSSFiles();
        timesStart.clear();
        timesEnd.clear();
        
        // LAYOUT THE APP
        initLayout();
        changeExportDir();

        // INIT THE EVENT HANDLERS
        initControllers();

        // SETUP FOOLPROOF DESIGN FOR THIS APP
        initFoolproofDesign();
    }

    // THIS HELPER METHOD INITIALIZES ALL THE CONTROLS IN THE WORKSPACE
    private void initLayout() {
        // FIRST LOAD THE FONT FAMILIES FOR THE COMBO BOX
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // THIS WILL BUILD ALL OF OUR JavaFX COMPONENTS FOR US
        AppGUIModule gui = app.getGUIModule();
        AppNodesBuilder csgBuilder = gui.getNodesBuilder();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
        
        

        
        
        

        // INIT THE HEADER ON THE LEFT
        
       
        VBox mainframe = csgBuilder.buildVBox(csg_LEFT_PANE, null, CLASS_csg_PANE, ENABLED);
       
        TabPane tabPane = csgBuilder.buildTabPane(csg_tabPane, mainframe, CLASS_csg_PANE, ENABLED);
        Tab site = csgBuilder.buildTab(csg_tab_site, tabPane, CLASS_csg_PANE, ENABLED);
        Tab syllabus = csgBuilder.buildTab(csg_tab_syllabus, tabPane, CLASS_csg_PANE, ENABLED);
        Tab meeting = csgBuilder.buildTab(csg_tab_meeting, tabPane, CLASS_csg_PANE, ENABLED);
        Tab oh = csgBuilder.buildTab(csg_tab_oh, tabPane, CLASS_csg_PANE, ENABLED);
        Tab schedule = csgBuilder.buildTab(csg_tab_schedule, tabPane, CLASS_csg_PANE, ENABLED);
        
        
        //build site pane below.
         ScrollPane siteScroll = new ScrollPane();
        int spacing = 10;
        
        VBox siteTab = csgBuilder.buildVBox(csg_site_tab_box, null, CLASS_csg_TAB_BOX, ENABLED);
        siteTab.setSpacing(spacing/2);
        siteTab.minHeightProperty().bind(tabPane.heightProperty().multiply(1));
        
        
        //set its size to be full tabpane width
        siteTab.prefWidthProperty().bind(tabPane.widthProperty().multiply(1.0));
        //Banner Section
        

        
        VBox banner = csgBuilder.buildVBox(csg_site_banner_box, null, CLASS_csg_BOX, ENABLED);
        csgBuilder.buildLabel(csg_site_banner_label, banner, CLASS_csg_HEADER_LABEL, ENABLED);
        HBox subNum = new HBox();
        
        subNum.setSpacing(spacing);
        
        Label subLab = csgBuilder.buildLabel(csg_site_subject_label, subNum, CLASS_csg_LABEL, ENABLED);
        ComboBox sub = csgBuilder.buildComboBox(csg_site_subject_combo, subjects  , "CSE",subNum, CLASS_csg_COMBO, ENABLED);
        sub.setEditable(true);
        Label numLab = csgBuilder.buildLabel(csg_site_number_label, subNum, CLASS_csg_LABEL, ENABLED);
        ComboBox num = csgBuilder.buildComboBox(csg_site_number_combo, numbers, "219", subNum, CLASS_csg_COMBO, ENABLED);
        num.setEditable(true);
        HBox semYear = new HBox();
        semYear.setSpacing(spacing);
        
        Label semLab = csgBuilder.buildLabel(csg_site_semester_label, semYear, CLASS_csg_LABEL, ENABLED);
        ComboBox sem = csgBuilder.buildComboBox(csg_site_semester_combo, semesters  , "Fall",semYear, CLASS_csg_COMBO, ENABLED);
        sem.setEditable(true);
        Label yearLab = csgBuilder.buildLabel(csg_site_year_label, semYear, CLASS_csg_LABEL, ENABLED);
        ComboBox year = csgBuilder.buildComboBox(csg_site_year_combo, years, "2018", semYear, CLASS_csg_COMBO, ENABLED);
        year.setEditable(true);
        HBox titleBox = new HBox();
        
        
        titleBox.setSpacing(spacing);
        
        Label titleLab = csgBuilder.buildLabel(csg_site_title_label, titleBox, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_site_title_textfield, titleBox, EMPTY_TEXT, ENABLED);
        
        HBox exportBox = new HBox();
        exportBox.setSpacing(spacing);
        csgBuilder.buildLabel(csg_site_export_label, exportBox, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildLabel(csg_site_exportVal_label, exportBox, EMPTY_TEXT, ENABLED); //just regular text
        
        banner.getChildren().addAll(subNum, semYear,titleBox,exportBox);
        banner.setSpacing(spacing);
        
        //Pages Section
        HBox pages = csgBuilder.buildHBox(csg_site_pages_box, null, CLASS_csg_BOX, ENABLED);
        pages.setSpacing(spacing);
        Label pageLab = csgBuilder.buildLabel(csg_site_pages_label, pages, CLASS_csg_HEADER_LABEL, ENABLED);
        CheckBox homeCheck = csgBuilder.buildCheckBox(csg_site_home_checkbox, pages, EMPTY_TEXT, ENABLED);
        CheckBox syllabusCheck = csgBuilder.buildCheckBox(csg_site_syllabus_checkbox, pages, EMPTY_TEXT, ENABLED);
        CheckBox scheudleCheck = csgBuilder.buildCheckBox(csg_site_schedule_checkbox, pages, EMPTY_TEXT, ENABLED);
        CheckBox hwCheck = csgBuilder.buildCheckBox(csg_site_hws_checkbox, pages, EMPTY_TEXT, ENABLED);

        //Style section
        ArrayList<Button> buttonList = new ArrayList<Button>();
        VBox style = csgBuilder.buildVBox(csg_site_style_box, null, CLASS_csg_BOX, ENABLED);
        csgBuilder.buildLabel(csg_site_style_label, style, CLASS_csg_HEADER_LABEL, ENABLED);
        style.setSpacing(spacing);
        HBox faviconBox = new HBox();
        faviconBox.setSpacing(spacing);
        csgBuilder.buildTextButton(csg_site_favicon_button, faviconBox, CLASS_csg_FILE_BUTTON, ENABLED);
        buttonList.add(csgBuilder.buildIconButton(csg_site_favicon_image, faviconBox, CLASS_csg_IMAGES, ENABLED));
        HBox navBox = new HBox();
        navBox.setSpacing(spacing);
        csgBuilder.buildTextButton(csg_site_navbar_button, navBox, CLASS_csg_FILE_BUTTON, ENABLED);
        buttonList.add(csgBuilder.buildIconButton(csg_site_navbar_image, navBox, CLASS_csg_IMAGES, ENABLED));
        HBox leftFootBox = new HBox();
        leftFootBox.setSpacing(spacing);
        csgBuilder.buildTextButton(csg_site_leftFooter_button, leftFootBox, CLASS_csg_FILE_BUTTON, ENABLED);
        buttonList.add(csgBuilder.buildIconButton(csg_site_leftFooter_image, leftFootBox, CLASS_csg_IMAGES, ENABLED));
        HBox rightFootBox = new HBox();
        rightFootBox.setSpacing(spacing);
        csgBuilder.buildTextButton(csg_site_rightFooter_button,  rightFootBox, CLASS_csg_FILE_BUTTON, ENABLED);
        buttonList.add(csgBuilder.buildIconButton(csg_site_rightFooter_image,  rightFootBox, CLASS_csg_IMAGES, ENABLED));
        
        for (int i = 0; i<buttonList.size(); i++) {
            Button b = buttonList.get(i);
            ImageView im = (ImageView)buttonList.get(i).getGraphic();
            im.setFitHeight(20);
            im.setFitWidth(100);
            b.setMinSize(99, 19);
            b.setMaxSize(100,20);
        }
       
        HBox cssBox = new HBox();
        cssBox.setSpacing(spacing);
        csgBuilder.buildLabel(csg_site_css_label, cssBox, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildComboBox(csg_site_css_combo, cssFiles, "", cssBox,  CLASS_csg_COMBO, ENABLED);
        
        Label cssNote = csgBuilder.buildLabel(csg_site_cssNote_label, null, CLASS_csg_BOLD, ENABLED);
        
        style.getChildren().addAll(faviconBox,navBox,leftFootBox,rightFootBox,cssBox,cssNote);
        
        //Instructor section
        VBox instructorBox = csgBuilder.buildVBox(csg_site_instructor_box, null, CLASS_csg_BOX, ENABLED);
        instructorBox.setSpacing(spacing);
        csgBuilder.buildLabel(csg_site_instructor_label, instructorBox, CLASS_csg_HEADER_LABEL, ENABLED);
        HBox nameRoom = new HBox();
        nameRoom.setSpacing(spacing);
        csgBuilder.buildLabel(csg_site_name_label, nameRoom, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_site_name_textfield, nameRoom, CLASS_csg_TEXTFIELD, ENABLED);
        
        csgBuilder.buildLabel(csg_site_room_label, nameRoom, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_site_room_textfield, nameRoom, CLASS_csg_TEXTFIELD, ENABLED);
        
        HBox emailHomePage = new HBox();
        emailHomePage.setSpacing(spacing);
        csgBuilder.buildLabel(csg_site_email_label, emailHomePage, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_site_email_textfield, emailHomePage, CLASS_csg_TEXTFIELD, ENABLED);
        
        csgBuilder.buildLabel(csg_site_homepage_label, emailHomePage, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_site_homepage_textfield, emailHomePage, CLASS_csg_TEXTFIELD, ENABLED);
        
        HBox addInstructor = new HBox();
        addInstructor.setSpacing(spacing);
        Button add = csgBuilder.buildIconButton(csg_site_addOh_button, addInstructor, EMPTY_TEXT, ENABLED);
        ((ImageView)add.getGraphic()).setFitHeight(15);
        ((ImageView)add.getGraphic()).setFitWidth(15);
        csgBuilder.buildLabel(csg_site_addOh_label, addInstructor, CLASS_csg_LABEL, ENABLED);
        
        TextArea officeHoursText = csgBuilder.buildTextArea(csg_site_oh_textarea, null, EMPTY_TEXT, ENABLED);
        officeHoursText.maxWidthProperty().bind(tabPane.widthProperty().multiply(0.75));
        officeHoursText.minWidthProperty().bind(tabPane.widthProperty().multiply(0.75));
        officeHoursText.setVisible(false);
        officeHoursText.setManaged(false);
       
        officeHoursText.resize(1, 1);
        
        instructorBox.getChildren().addAll(nameRoom, emailHomePage,addInstructor, officeHoursText);
        
        siteTab.getChildren().addAll(banner,pages,style, instructorBox);
        
        //set content and scroll policy of scrollPane within tab
        siteScroll.setHbarPolicy(ScrollBarPolicy.NEVER);
        siteScroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        siteScroll.setContent(siteTab);
        
        //add scrollpane to tab
        site.setContent(siteScroll);
        
        
        
        //build syllabus pane below
        VBox syllabusBox = csgBuilder.buildVBox(csg_syllabus_tab_box, null, CLASS_csg_TAB_BOX, ENABLED);
        syllabusBox.setSpacing(spacing/2);
        ScrollPane syllabusScroll = new ScrollPane();
        syllabusBox.prefWidthProperty().bind(tabPane.widthProperty().multiply(1));
        syllabusBox.minHeightProperty().bind(tabPane.heightProperty().multiply(1));
        syllabusScroll.setContent(syllabusBox);
        for (int i =0; i<syllabusItemPropertyNames.size(); i++) {
            String itemName = syllabusItemPropertyNames.get(i);
            String boxId = "csg_syllabus_"+itemName+"_box";
            String buttonId = "csg_syllabus_"+itemName+"_button";
            String labelId = "csg_syllabus_"+itemName+"_label";
            String textAreaId = "csg_syllabus_"+itemName+"_textarea";
            
            VBox bigBox = csgBuilder.buildVBox(boxId, syllabusBox, CLASS_csg_BOX, ENABLED);
            HBox hb = new HBox();
            Button butt= csgBuilder.buildIconButton(buttonId, hb, EMPTY_TEXT, ENABLED);
            ((ImageView)butt.getGraphic()).setFitHeight(15);
            ((ImageView)butt.getGraphic()).setFitWidth(15);
            csgBuilder.buildLabel(labelId, hb, CLASS_csg_LABEL, ENABLED);
            TextArea area = csgBuilder.buildTextArea(textAreaId, null, EMPTY_TEXT, ENABLED);
            area.maxWidthProperty().bind(tabPane.widthProperty().multiply(0.75));
            area.minWidthProperty().bind(tabPane.widthProperty().multiply(0.75));
            area.setVisible(false);
            area.setManaged(false);
            bigBox.getChildren().addAll(hb,area);
        }
        syllabusScroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        syllabusScroll.setHbarPolicy(ScrollBarPolicy.NEVER);
        
        syllabus.setContent(syllabusScroll);
        
        
        
        //build meeting pane below
        ScrollPane meetingScroll = new ScrollPane();
        VBox meetingBox = new VBox();
        meetingScroll.setContent(meetingBox);
        meetingBox.prefWidthProperty().bind(meetingScroll.widthProperty().multiply(0.99));
        meetingScroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        meetingScroll.setHbarPolicy(ScrollBarPolicy.NEVER);
        VBox lectureBox = csgBuilder.buildVBox(csg_meeting_lectures_box, meetingBox, CLASS_csg_BOX, ENABLED);
        
        //lecture section
        HBox addRemoveLec = new HBox();
        Button addLec = csgBuilder.buildIconButton(csg_meeting_lectures_add_button, addRemoveLec, EMPTY_TEXT, ENABLED);
        Button remLec = csgBuilder.buildIconButton(csg_meeting_lectures_remove_button, addRemoveLec, EMPTY_TEXT, ENABLED);
        
        //sets appropriate size for buttons
        
        ((ImageView)addLec.getGraphic()).setFitHeight(10);
        ((ImageView)addLec.getGraphic()).setFitWidth(10);
        ((ImageView)remLec.getGraphic()).setFitHeight(10);
        ((ImageView)remLec.getGraphic()).setFitWidth(10);
        
        
        csgBuilder.buildLabel(csg_meeting_lectures_label, addRemoveLec, CLASS_csg_LABEL, ENABLED);

        lectureBox.getChildren().add(addRemoveLec);
        
        //makes lecture table
        TableView<TeachingAssistantPrototype> lectureTable = csgBuilder.buildTableView(csg_meeting_lectures_table, lectureBox, CLASS_csg_TABLE_VIEW, ENABLED);
        lectureTable.getSelectionModel().setCellSelectionEnabled(ENABLED);
        lectureTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lectureTable.setEditable(ENABLED);
        
        TableColumn lecSectionColumnn = csgBuilder.buildTableColumn(csg_meeting_lectures_section_column, lectureTable, CLASS_csg_COLUMN);
        lecSectionColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        lecSectionColumnn.prefWidthProperty().bind(lectureTable.widthProperty().multiply(1.0/4.0));
        lecSectionColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn lecDaysColumnn = csgBuilder.buildTableColumn(csg_meeting_lectures_days_column, lectureTable, CLASS_csg_COLUMN);
        lecDaysColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("days"));
        lecDaysColumnn.prefWidthProperty().bind(lectureTable.widthProperty().multiply(1.0/4.0));
        lecDaysColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn lecTimeColumnn = csgBuilder.buildTableColumn(csg_meeting_lectures_time_column, lectureTable, CLASS_csg_COLUMN);
        lecTimeColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("time"));
        lecTimeColumnn.prefWidthProperty().bind(lectureTable.widthProperty().multiply(1.0/4.0));
        lecTimeColumnn.setCellFactory(TextFieldTableCell.forTableColumn());

        TableColumn lecRoomColumnn = csgBuilder.buildTableColumn(csg_meeting_lectures_room_column, lectureTable, CLASS_csg_COLUMN);
        lecRoomColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        lecRoomColumnn.prefWidthProperty().bind(lectureTable.widthProperty().multiply(1.0/4.0));
        lecRoomColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        
        
        //recitation section
        VBox recBox = csgBuilder.buildVBox(csg_meeting_rec_box, meetingBox, CLASS_csg_BOX, ENABLED);
        HBox addRemoveRec = new HBox();
        Button addRec = csgBuilder.buildIconButton(csg_meeting_rec_add_button, addRemoveRec, EMPTY_TEXT, ENABLED);
        Button remRec = csgBuilder.buildIconButton(csg_meeting_rec_remove_button, addRemoveRec, EMPTY_TEXT, ENABLED);
        
        //sets appropriate size for buttons
        
        ((ImageView)addRec.getGraphic()).setFitHeight(10);
        ((ImageView)addRec.getGraphic()).setFitWidth(10);
        ((ImageView)remRec.getGraphic()).setFitHeight(10);
        ((ImageView)remRec.getGraphic()).setFitWidth(10);
        
        
        csgBuilder.buildLabel(csg_meeting_rec_label, addRemoveRec, CLASS_csg_LABEL, ENABLED);

        recBox.getChildren().add(addRemoveRec);
        
        //makes rec table
        TableView<TeachingAssistantPrototype> recTable = csgBuilder.buildTableView(csg_meeting_rec_table, recBox, CLASS_csg_TABLE_VIEW, ENABLED);
        recTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        recTable.getSelectionModel().setCellSelectionEnabled(ENABLED);
        recTable.setEditable(ENABLED);
        TableColumn recSectionColumnn = csgBuilder.buildTableColumn(csg_meeting_rec_section_column, recTable, CLASS_csg_COLUMN);
        recSectionColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        recSectionColumnn.prefWidthProperty().bind(recTable.widthProperty().multiply(1.0/10.0));
        recSectionColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn recDayTimeColumnn = csgBuilder.buildTableColumn(csg_meeting_rec_dayTime_column, recTable, CLASS_csg_COLUMN);
        recDayTimeColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("daysAndTime"));
        recDayTimeColumnn.prefWidthProperty().bind(recTable.widthProperty().multiply(3.0/10.0));
        recDayTimeColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn recRoomColumnn = csgBuilder.buildTableColumn(csg_meeting_rec_room_column, recTable, CLASS_csg_COLUMN);
        recRoomColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        recRoomColumnn.prefWidthProperty().bind(recTable.widthProperty().multiply(2.0/10.0));
        recRoomColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn rectaOneColumnn = csgBuilder.buildTableColumn(csg_meeting_rec_taOne_column, recTable, CLASS_csg_COLUMN);
        rectaOneColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("taOne"));
        rectaOneColumnn.prefWidthProperty().bind(recTable.widthProperty().multiply(2.0/10.0));
        rectaOneColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn rectaTwoColumnn = csgBuilder.buildTableColumn(csg_meeting_rec_taTwo_column, recTable, CLASS_csg_COLUMN);
        rectaTwoColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("taTwo"));
        rectaTwoColumnn.prefWidthProperty().bind(recTable.widthProperty().multiply(2.0/10.0));
        rectaTwoColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        //lab section
        VBox labBox = csgBuilder.buildVBox(csg_meeting_lab_box, meetingBox, CLASS_csg_BOX, ENABLED);
        HBox addRemoveLab = new HBox();
        Button addLab = csgBuilder.buildIconButton(csg_meeting_lab_add_button, addRemoveLab, EMPTY_TEXT, ENABLED);
        Button remLab = csgBuilder.buildIconButton(csg_meeting_lab_remove_button, addRemoveLab, EMPTY_TEXT, ENABLED);
        
        //sets appropriate size for buttons
        
        ((ImageView)addLab.getGraphic()).setFitHeight(10);
        ((ImageView)addLab.getGraphic()).setFitWidth(10);
        ((ImageView)remLab.getGraphic()).setFitHeight(10);
        ((ImageView)remLab.getGraphic()).setFitWidth(10);
        
        
        csgBuilder.buildLabel(csg_meeting_lab_label, addRemoveLab, CLASS_csg_LABEL, ENABLED);

        labBox.getChildren().add(addRemoveLab);
        
        //makes lab table
        TableView<TeachingAssistantPrototype> labTable = csgBuilder.buildTableView(csg_meeting_lab_table, labBox, CLASS_csg_TABLE_VIEW, ENABLED);
        labTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        labTable.getSelectionModel().setCellSelectionEnabled(ENABLED);
        labTable.setEditable(ENABLED);
        TableColumn labSectionColumnn = csgBuilder.buildTableColumn(csg_meeting_lab_section_column, labTable, CLASS_csg_COLUMN);
        labSectionColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("section"));
        labSectionColumnn.prefWidthProperty().bind(labTable.widthProperty().multiply(1.0/10.0));
        labSectionColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn labDayTimeColumnn = csgBuilder.buildTableColumn(csg_meeting_lab_dayTime_column, labTable, CLASS_csg_COLUMN);
        labDayTimeColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("daysAndTime"));
        labDayTimeColumnn.prefWidthProperty().bind(labTable.widthProperty().multiply(3.0/10.0));
        labDayTimeColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn labRoomColumnn = csgBuilder.buildTableColumn(csg_meeting_lab_room_column, labTable, CLASS_csg_COLUMN);
        labRoomColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("room"));
        labRoomColumnn.prefWidthProperty().bind(labTable.widthProperty().multiply(2.0/10.0));
        labRoomColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn labtaOneColumnn = csgBuilder.buildTableColumn(csg_meeting_lab_taOne_column, labTable, CLASS_csg_COLUMN);
        labtaOneColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("taOne"));
        labtaOneColumnn.prefWidthProperty().bind(labTable.widthProperty().multiply(2.0/10.0));
        labtaOneColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        TableColumn labtaTwoColumnn = csgBuilder.buildTableColumn(csg_meeting_lab_taTwo_column, labTable, CLASS_csg_COLUMN);
        labtaTwoColumnn.setCellValueFactory(new PropertyValueFactory<String, String>("taTwo"));
        labtaTwoColumnn.prefWidthProperty().bind(labTable.widthProperty().multiply(2.0/10.0));
        labtaTwoColumnn.setCellFactory(TextFieldTableCell.forTableColumn());
        
        
        
        
        
        
        
        
        
        meeting.setContent(meetingScroll);

        //build oh pane below
        VBox ohBox = csgBuilder.buildVBox(csg_oh_box, null, CLASS_csg_BOX, ENABLED);
        ohBox.setSpacing(spacing);
        //ta types
        HBox tasHeaderBox = csgBuilder.buildHBox(csg_TAS_HEADER_PANE, ohBox, CLASS_csg_BOX, ENABLED);
        tasHeaderBox.setSpacing(20);
        
        Button b =csgBuilder.buildIconButton(csg_oh_removeTA_button, tasHeaderBox, EMPTY_TEXT, ENABLED);
        csgBuilder.changeIcon(b,csg_oh_removeTA_button ,"MINUS" , 10, 10);
        
//        csgBuilder.buildLabel(csg_oh_removeTA_label, tasHeaderBox, CLASS_csg_LABEL, ENABLED);
//        
        csgBuilder.buildLabel(csg_TAS_HEADER_LABEL, tasHeaderBox, CLASS_csg_LABEL, ENABLED);
        ToggleGroup taType = new ToggleGroup();
        HBox tasTypeBox = new HBox();
        tasTypeBox.setSpacing(spacing);
        tasHeaderBox.getChildren().add(tasTypeBox);
        tasTypeBox.setSpacing(5);
        csgBuilder.buildRadioButton((Object)csg_TAS_TYPE_ALL, tasTypeBox,CLASS_csg_RADIO,taType, ENABLED);
        csgBuilder.buildRadioButton((Object)csg_TAS_TYPE_UG, tasTypeBox,CLASS_csg_RADIO,taType, ENABLED);
        csgBuilder.buildRadioButton((Object)csg_TAS_TYPE_G, tasTypeBox,CLASS_csg_RADIO,taType, ENABLED);
        
        
        
        
        
        TableView<TeachingAssistantPrototype> taTable = csgBuilder.buildTableView(csg_TAS_TABLE_VIEW, ohBox, CLASS_csg_TABLE_VIEW, ENABLED);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        
        TableColumn nameColumn = csgBuilder.buildTableColumn(csg_NAME_TABLE_COLUMN, taTable, CLASS_csg_COLUMN);
        nameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        nameColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/4.0));
        
        TableColumn emailColumn = csgBuilder.buildTableColumn(csg_EMAIL_TABLE_COLUMN, taTable, CLASS_csg_COLUMN);
        emailColumn.setCellValueFactory(new PropertyValueFactory<String, String>("email"));
        emailColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/2.5));
        
        TableColumn slotsColumn = csgBuilder.buildTableColumn(csg_SLOTS_TABLE_COLUMN, taTable, CLASS_csg_COLUMN);
        slotsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("timeSlot"));
        slotsColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/6.67));
        
        TableColumn typeColumn = csgBuilder.buildTableColumn(csg_TYPE_TABLE_COLUMN, taTable, CLASS_csg_COLUMN);
        typeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        typeColumn.prefWidthProperty().bind(taTable.widthProperty().multiply(1.0/5.0));
        
        HBox taBox = csgBuilder.buildHBox(csg_ADD_TA_PANE, ohBox, CLASS_csg_PANE, ENABLED);
        taBox.setSpacing(spacing);
        csgBuilder.buildTextField(csg_NAME_TEXT_FIELD, taBox, CLASS_csg_TEXT_FIELD, ENABLED);
        csgBuilder.buildTextField(csg_EMAIL_TEXT_FIELD, taBox, CLASS_csg_TEXT_FIELD, ENABLED);
        csgBuilder.buildTextButton(csg_ADD_TA_BUTTON, taBox, CLASS_csg_BUTTON, ENABLED);
        
        HBox labelTime = new HBox();
        labelTime.setSpacing(spacing*10);
        HBox timeBox = new HBox();
        
        timeBox.setSpacing(spacing);
        csgBuilder.buildLabel(csg_oh_oh_label, labelTime, CLASS_csg_LABEL, ENABLED);
        labelTime.getChildren().add(timeBox);
        csgBuilder.buildLabel(csg_oh_start_label, timeBox, CLASS_csg_LABEL, ENABLED);
        ComboBox startBox =  csgBuilder.buildComboBox(csg_oh_start_combo, timesStart, "9:00am", timeBox, CLASS_csg_COMBO, ENABLED);
        
        csgBuilder.buildLabel(csg_oh_end_label, timeBox, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildComboBox(csg_oh_end_combo, timesEnd, "10:00pm", timeBox, CLASS_csg_COMBO, ENABLED);
        
        //create listener for end combobox to depend on starts current value
        
        
        
        startBox.valueProperty().addListener(e->{
            String start =startBox.valueProperty().getValue().toString();
            boolean after = false;
            timesEnd.clear();
            for (int i =0; i<timesStart.size(); i++) {
                if (timesStart.get(i).equals(start)) {
                    after = true;
                }
                if(after==true) {
                    timesEnd.add(timesStart.get(i));
                }
            }
            
        });
        
        ohBox.getChildren().add(labelTime);
        
        
        
        
                //sets prompttext
        nameColumn.textProperty().addListener((observable, oldValue, newValue) -> {
            ((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)).setPromptText(newValue);
        });
        emailColumn.textProperty().addListener((observable, oldValue, newValue) -> {
            ((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)).setPromptText(newValue);
        });
        
        
        
         // SETUP THE OFFICE HOURS TABLE
        TableView<TimeSlot> officeHoursTable = csgBuilder.buildTableView(csg_OFFICE_HOURS_TABLE_VIEW, ohBox, CLASS_csg_OFFICE_HOURS_TABLE_VIEW, ENABLED);
        TableColumn startTimeColumn = csgBuilder.buildTableColumn(csg_START_TIME_TABLE_COLUMN, officeHoursTable, CLASS_csg_TIME_COLUMN);
        TableColumn endTimeColumn = csgBuilder.buildTableColumn(csg_END_TIME_TABLE_COLUMN, officeHoursTable, CLASS_csg_TIME_COLUMN);
        TableColumn mondayColumn = csgBuilder.buildTableColumn(csg_MONDAY_TABLE_COLUMN, officeHoursTable, CLASS_csg_DAY_OF_WEEK_COLUMN);
        TableColumn tuesdayColumn = csgBuilder.buildTableColumn(csg_TUESDAY_TABLE_COLUMN, officeHoursTable, CLASS_csg_DAY_OF_WEEK_COLUMN);
        TableColumn wednesdayColumn = csgBuilder.buildTableColumn(csg_WEDNESDAY_TABLE_COLUMN, officeHoursTable, CLASS_csg_DAY_OF_WEEK_COLUMN);
        TableColumn thursdayColumn = csgBuilder.buildTableColumn(csg_THURSDAY_TABLE_COLUMN, officeHoursTable, CLASS_csg_DAY_OF_WEEK_COLUMN);
        TableColumn fridayColumn = csgBuilder.buildTableColumn(csg_FRIDAY_TABLE_COLUMN, officeHoursTable, CLASS_csg_DAY_OF_WEEK_COLUMN);
        startTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("startTime"));
        endTimeColumn.setCellValueFactory(new PropertyValueFactory<String, String>("endTime"));
        mondayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("monday"));
        tuesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("tuesday"));
        wednesdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("wednesday"));
        thursdayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("thursday"));
        fridayColumn.setCellValueFactory(new PropertyValueFactory<String, String>("friday"));
        officeHoursTable.getSelectionModel().setCellSelectionEnabled(true);
        
        officeHoursTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
            ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/7.0));
        }
        // MAKE SURE IT'S THE TABLE THAT ALWAYS GROWS IN THE LEFT PANE
        VBox.setVgrow(officeHoursTable, Priority.ALWAYS);
        
        
        
        oh.setContent(ohBox);
        
        //build schedule pane below
        ScrollPane scheduleScroll = new ScrollPane();
        VBox scheduleTab = csgBuilder.buildVBox(csg_schedule_box, null, CLASS_csg_TAB_BOX, ENABLED);
        scheduleTab.setSpacing(spacing/2);
        VBox calenderBox = csgBuilder.buildVBox(csg_schedule_calender_box, scheduleTab, CLASS_csg_BOX, ENABLED);
        calenderBox.setSpacing(spacing);
        csgBuilder.buildLabel(csg_schedule_calender_label, calenderBox, CLASS_csg_HEADER_LABEL, ENABLED);
        HBox calenderHBox = new HBox();
        calenderHBox.setSpacing(spacing);
        calenderBox.getChildren().add(calenderHBox);
        csgBuilder.buildLabel(csg_schedule_monday_label, calenderHBox, EMPTY_TEXT, ENABLED);
        csgBuilder.buildDatePicker(csg_schedule_monday_date, calenderHBox, EMPTY_TEXT, ENABLED);
        csgBuilder.buildLabel(csg_schedule_friday_label, calenderHBox, EMPTY_TEXT, ENABLED);
        csgBuilder.buildDatePicker(csg_schedule_friday_date, calenderHBox, EMPTY_TEXT, ENABLED);
        
        VBox scheduleBox = csgBuilder.buildVBox(csg_schedule_items_box, scheduleTab, CLASS_csg_BOX, ENABLED);
        scheduleBox.setSpacing(spacing);
        HBox scheduleHeader = new HBox();
        scheduleHeader.setSpacing(spacing);
        scheduleBox.getChildren().add(scheduleHeader);
        Button scheduleSub = csgBuilder.buildIconButton(csg_schedule_items_button, scheduleHeader, EMPTY_TEXT, ENABLED);
        ((ImageView)scheduleSub.getGraphic()).setFitHeight(10);
        ((ImageView)scheduleSub.getGraphic()).setFitWidth(10);
        csgBuilder.buildLabel(csg_schedule_items_label, scheduleHeader, CLASS_csg_HEADER_LABEL, ENABLED);
        
        TableView itemTable = csgBuilder.buildTableView(csg_schedule_items_table, scheduleBox, CLASS_csg_TABLE_VIEW, ENABLED);
        
        TableColumn itemType = csgBuilder.buildTableColumn(csg_schedule_items_type_column, itemTable, CLASS_csg_COLUMN);
        itemType.setCellValueFactory(new PropertyValueFactory<String, String>("type"));
        itemType.prefWidthProperty().bind(lectureTable.widthProperty().multiply(1.0/5.0));
        
        TableColumn itemDate = csgBuilder.buildTableColumn(csg_schedule_items_date_column, itemTable, CLASS_csg_COLUMN);
        itemDate.setCellValueFactory(new PropertyValueFactory<String, String>("date"));
        itemDate.prefWidthProperty().bind(lectureTable.widthProperty().multiply(1.0/5.0));

        TableColumn itemTitle = csgBuilder.buildTableColumn(csg_schedule_items_title_column, itemTable, CLASS_csg_COLUMN);
        itemTitle.setCellValueFactory(new PropertyValueFactory<String, String>("title"));
        itemTitle.prefWidthProperty().bind(lectureTable.widthProperty().multiply(1.0/5.0));
        
        TableColumn itemTopic = csgBuilder.buildTableColumn(csg_schedule_items_topic_column, itemTable, CLASS_csg_COLUMN);
        itemTopic.setCellValueFactory(new PropertyValueFactory<String, String>("topic"));
        itemTopic.prefWidthProperty().bind(lectureTable.widthProperty().multiply(2.0/5.0));
        
        
        VBox addBox = csgBuilder.buildVBox(csg_schedule_add_box, scheduleTab, CLASS_csg_BOX, ENABLED);
        csgBuilder.buildLabel(csg_schedule_add_label, addBox, CLASS_csg_HEADER_LABEL, ENABLED);
        addBox.setSpacing(spacing);
        
        HBox addType = new HBox();
        csgBuilder.buildLabel(csg_schedule_type_label, addType, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildComboBox(csg_schedule_type_combo ,typeOptions, "Lecture", addType, CLASS_csg_COMBO, ENABLED);
        
        HBox addDate = new HBox();
        csgBuilder.buildLabel(csg_schedule_date_label, addDate, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildDatePicker(csg_schedule_date_date, addDate, EMPTY_TEXT, ENABLED);
        
        HBox addTitle = new HBox();
        csgBuilder.buildLabel(csg_schedule_title_label, addTitle, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_schedule_title_textfield, addTitle, CLASS_csg_TEXTFIELD, ENABLED);

        HBox addTopic = new HBox();
        csgBuilder.buildLabel(csg_schedule_topic_label, addTopic, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_schedule_topic_textfield, addTopic, CLASS_csg_TEXTFIELD, ENABLED);

        HBox addLink = new HBox();
        csgBuilder.buildLabel(csg_schedule_link_label, addLink, CLASS_csg_LABEL, ENABLED);
        csgBuilder.buildTextField(csg_schedule_link_textfield, addLink, CLASS_csg_TEXTFIELD, ENABLED);
        
        
        
        addBox.getChildren().addAll(addType,addDate,addTitle,addTopic,addLink);
        csgBuilder.buildTextButton(csg_schedule_add_button, addBox, CLASS_csg_BUTTON, ENABLED);
        addType.setSpacing(spacing);
        addDate.setSpacing(spacing);
        addTitle.setSpacing(spacing);
        addTopic.setSpacing(spacing);
        addLink.setSpacing(spacing);
        
        
        
        scheduleScroll.setContent(scheduleTab);
        scheduleScroll.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scheduleScroll.setHbarPolicy(ScrollBarPolicy.NEVER);
        
        
        schedule.setContent(scheduleScroll);
        
        
        //add to tabpane
        tabPane.getTabs().addAll(site,syllabus,meeting,oh,schedule);
        
        //set width property of tabpane
        tabPane.prefWidthProperty().bind(gui.getWindow().widthProperty().multiply(0.99));
        
        tabPane.tabMaxWidthProperty().bind(tabPane.widthProperty().multiply(1/5.25));
        tabPane.tabMinWidthProperty().bind(tabPane.widthProperty().multiply(1/5.5));
        
        
        //init ta popup stage
        spacing = 15;
        String labelStyle = " -fx-text-fill: black; -fx-font-weight: bold; -fx-font-size: 12pt;";
        VBox layout = csgBuilder.buildVBox(csg_EDIT_TA_BOX, null, CLASS_csg_EDIT_TA_BOX, ENABLED);
        layout.setSpacing(spacing);
        layout.setStyle("-fx-border-color: white; -fx-border-width:  4; -fx-background-color: #DDE9E8;");
        Label title = csgBuilder.buildLabel(csg_EDIT_TA_LABEL, layout, null, ENABLED);
        title.setStyle( "-fx-text-fill: black;  -fx-font-weight: bold; -fx-font-size: 16pt;");
        HBox nameH = csgBuilder.buildHBox(csg_EDIT_TA_NAME_BOX, layout, CLASS_csg_PANE, ENABLED);
        nameH.setSpacing(spacing);
        HBox emailH = csgBuilder.buildHBox(csg_EDIT_TA_EMAIL_BOX, layout, CLASS_csg_PANE, ENABLED);
        emailH.setSpacing(spacing);
        HBox typeH = csgBuilder.buildHBox(csg_EDIT_TA_TYPE_BOX, layout, CLASS_csg_PANE, ENABLED);
        typeH.setSpacing(spacing);
        HBox buttonH = csgBuilder.buildHBox(csg_EDIT_TA_BUTTON_BOX, layout, CLASS_csg_PANE, ENABLED);
        buttonH.setSpacing(1);
        
        //make export dir text warning. This node itself will not be
        //accessible, but is used to change the export dir node/
        csgBuilder.buildLabel(csg_site_export_dir_warning, null, EMPTY_TEXT, ENABLED)
;        
        
        //add all to workspace
        workspace = new BorderPane();
        ((BorderPane)workspace).setCenter(mainframe);



        //layout.setAlignment(Pos.CENTER);
        csgBuilder.buildLabel(csg_EDIT_TA_NAME, nameH, CLASS_csg_LABEL, ENABLED).setStyle(labelStyle);
        csgBuilder.buildTextField(csg_EDIT_TA_NAME_TEXT_FIELD, nameH, CLASS_csg_TEXT_FIELD, ENABLED);
        
        csgBuilder.buildLabel(csg_EDIT_TA_EMAIL, emailH, CLASS_csg_LABEL, ENABLED).setStyle(labelStyle);
        csgBuilder.buildTextField(csg_EDIT_TA_EMAIL_TEXT_FIELD, emailH, CLASS_csg_TEXT_FIELD, ENABLED);
        
        csgBuilder.buildLabel(csg_EDIT_TA_TYPE, typeH, CLASS_csg_LABEL, ENABLED).setStyle(labelStyle); //reusing language settings
        ToggleGroup g = new ToggleGroup();
        csgBuilder.buildRadioButton(csg_EDIT_TA_UNDERGRADUATE_RADIO,typeH,CLASS_csg_RADIO,g, ENABLED);
        csgBuilder.buildRadioButton(csg_EDIT_TA_GRADUATE_RADIO,typeH,CLASS_csg_RADIO,g, ENABLED);
        buttonH.setAlignment(Pos.CENTER);
        csgBuilder.buildTextButton(csg_EDIT_TA_OK_BUTTON, buttonH,CLASS_csg_BUTTON , ENABLED).setStyle("-fx-font-weight: bold;");
        csgBuilder.buildTextButton(csg_EDIT_TA_CANCEL_BUTTON, buttonH,CLASS_csg_BUTTON , ENABLED).setStyle("-fx-font-weight: bold;");
        Scene sceneTA= new Scene(layout, 370,250 );
        taWindow.setScene(sceneTA);
    }
    private void initControllers() {
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorController controller = new CourseSiteGeneratorController((CourseSiteGeneratorApp) app);
        
         
       
        
        
        AppNodesBuilder csgBuilder = gui.getNodesBuilder();
        CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();  
        
        //ta window controllers
        
        
        ((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)).textProperty().addListener((observable) ->{
                       CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
            TeachingAssistantPrototype ta = check.getSelectedTA();
             String newName = ((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)).getText();
             String newEmail = ((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)).getText();
        boolean name = nameValid(newName,((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)))||newName.equals(ta.getName());
        if(name) ((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        boolean email = emailValid(newEmail,((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)))||newEmail.equals(ta.getEmail());
        if(email) ((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        if (name&&email) {
            ((Button)gui.getGUINode(csg_EDIT_TA_OK_BUTTON)).setDisable(false);
        }
        else {
           ((Button)gui.getGUINode(csg_EDIT_TA_OK_BUTTON)).setDisable(true); 
        }
        
        });
        ((Button)gui.getGUINode(csg_EDIT_TA_CANCEL_BUTTON)).setOnAction(e->{
            taWindow.hide();
        });
       
        ((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)).textProperty().addListener((observable) ->{
            CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
            TeachingAssistantPrototype ta = check.getSelectedTA();
             String newName = ((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)).getText();
             String newEmail = ((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)).getText();
        boolean name = nameValid(newName,((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)))||newName.equals(ta.getName());
        if(name) ((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        boolean email = emailValid(newEmail,((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)))||newEmail.equals(ta.getEmail());
        if(email) ((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)).setStyle("-fx-text-inner-color: black;");
        if (name&&email) {
            ((Button)gui.getGUINode(csg_EDIT_TA_OK_BUTTON)).setDisable(false);
        }
        else {
           ((Button)gui.getGUINode(csg_EDIT_TA_OK_BUTTON)).setDisable(true); 
        }
        
        });
        
        
        
        ((Button)gui.getGUINode(csg_EDIT_TA_OK_BUTTON)).setOnAction(e-> {
         CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
        TeachingAssistantPrototype ta = check.getSelectedTA();
        
        String newName = ((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)).getText();
        String newEmail = ((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)).getText();
        String newType;
        

        
        boolean ugSelected = ((RadioButton)gui.getGUINode(csg_EDIT_TA_UNDERGRADUATE_RADIO)).isSelected();
        System.out.println(ugSelected);
        if (ugSelected) {
            newType = "undergraduate";
        }
        else {
            newType = "graduate";
        }
        ADD_EDIT_TA_Transcation trans = new ADD_EDIT_TA_Transcation(check,gui,ta,newName,newEmail,newType);
        app.processTransaction(trans);
        taWindow.hide();
        String currType = check.getCurrentType();
        if (currType.equals("all")) {
            
        }
        else {
            currType = newType;
        }
        check.populateDisplayedList(currType);
        });
        
        
        
        
        
        
        //end of ta window controllers

        
        //language controllers for timeslots
        ((RadioButton)gui.getGUINode(csg_TAS_TYPE_UG)).textProperty().addListener((observable, oldValue, newValue)->{
            
            CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
            check.updateTimeSlotLanguages(oldValue, newValue);
            check.populateDisplayedList(check.getCurrentType());
        });
        ((RadioButton)gui.getGUINode(csg_TAS_TYPE_G)).textProperty().addListener((observable, oldValue, newValue)->{
            CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
            check.updateTimeSlotLanguages(oldValue, newValue);
            check.populateDisplayedList(check.getCurrentType());
        });
        
        //Site tab
        
        ArrayList<ComboBox> bannerCombos = new ArrayList<ComboBox>(){{
        add((ComboBox)gui.getGUINode(csg_site_subject_combo));
        add((ComboBox)gui.getGUINode(csg_site_number_combo));
        add((ComboBox)gui.getGUINode(csg_site_number_combo));
        add((ComboBox)gui.getGUINode(csg_site_semester_combo));
        add((ComboBox)gui.getGUINode(csg_site_year_combo));
}};
        for (int i=0; i<bannerCombos.size(); i++) {
            ComboBox b = bannerCombos.get(i);
            b.getSelectionModel().selectedIndexProperty().addListener(e->{
                changeExportDir();
            });   
        }
                ArrayList<CheckBox> pageChecks = new ArrayList<CheckBox>(){{
        add((CheckBox)gui.getGUINode(csg_site_home_checkbox));
        add((CheckBox)gui.getGUINode(csg_site_syllabus_checkbox));
        add((CheckBox)gui.getGUINode(csg_site_schedule_checkbox));
        add((CheckBox)gui.getGUINode(csg_site_hws_checkbox));
}};
         for (int i = 0; i<pageChecks.size(); i++) {
            CheckBox b = pageChecks.get(i);
            b.selectedProperty().addListener((observable,oldVal,newVal)->{
                if (!b.isFocused()) return;
                String oldVa = ""+oldVal.booleanValue();
                String newVa = ""+newVal.booleanValue();
                AddEdit_Site_Transaction trans = new AddEdit_Site_Transaction((CourseSiteGeneratorData)app.getDataComponent(), b, "checkbox",oldVa,newVa);
                app.processTransaction(trans);
                changeExportDir();
            });   
         }
         
        
        //start and end time comboBoxes
        
        ((ComboBox)gui.getGUINode(csg_oh_start_combo)).valueProperty().addListener(e->{
            CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
            String start =((ComboBox)gui.getGUINode(csg_oh_start_combo)).valueProperty().getValue().toString();
            String end = ""+MAX_END_HOUR;
            if (((ComboBox)gui.getGUINode(csg_oh_end_combo)).valueProperty().getValue()!=null)
            end = ((ComboBox)gui.getGUINode(csg_oh_end_combo)).valueProperty().getValue().toString();
            check.switchToTime(start, end);
        
        });
          ((ComboBox)gui.getGUINode(csg_oh_end_combo)).valueProperty().addListener(e->{
            CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
            String start =((ComboBox)gui.getGUINode(csg_oh_start_combo)).valueProperty().getValue().toString();
            String end = ((ComboBox)gui.getGUINode(csg_oh_end_combo)).valueProperty().getValue().toString();
            check.switchToTime(start, end);
        
        });
        
        
        ((Button)gui.getGUINode(csg_site_favicon_button)).setOnAction(e->{
            
           openImageDialog("favicon");
        });
        ((Button)gui.getGUINode(csg_site_navbar_button)).setOnAction(e->{
           openImageDialog("navbar");
        });
        ((Button)gui.getGUINode(csg_site_leftFooter_button)).setOnAction(e->{
           openImageDialog("leftFooter");
        });
        ((Button)gui.getGUINode(csg_site_rightFooter_button)).setOnAction(e->{
           openImageDialog("rightFooter");
        });
        
        
        
        
        
        
        TableView oh = (TableView)gui.getGUINode(csg_OFFICE_HOURS_TABLE_VIEW);
        
        RadioButton all = ((RadioButton )gui.getGUINode(csg_TAS_TYPE_ALL));
        RadioButton undergraduate = ((RadioButton )gui.getGUINode(csg_TAS_TYPE_UG));
        RadioButton graduate = ((RadioButton )gui.getGUINode(csg_TAS_TYPE_G));
        ToggleGroup tasType = all.getToggleGroup();
        
        all.setOnAction(e->{
           ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(true);
           oh.getSelectionModel().clearSelection();
           switchToType("all");
           resizecsg();
    
        });
        undergraduate.setOnAction(e->{
            String nameTxt = ((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)).getText();
            String emailTxt = ((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)).getText();
            oh.getSelectionModel().clearSelection();
            switchToType("undergraduate");
            if (nameValid(nameTxt,((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD))) && emailValid(emailTxt,((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)))) {
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(true);
            }
            resizecsg();
        });
        graduate.setOnAction(e->{
            String nameTxt = ((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)).getText();
            String emailTxt = ((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)).getText();
            oh.getSelectionModel().clearSelection();
            switchToType("graduate");
            if (nameValid(nameTxt,((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD))) && emailValid(emailTxt,((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)))) {
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(true);
            }
            resizecsg();
        });
        
        
        
        ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(true);
        ((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)).setOnAction(e -> {
            
        });
        
        
        
        ((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)).textProperty().addListener((observable) -> {
            String emailTxt = ((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)).getText();
            String nameTxt = ((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)).getText();
            boolean name= nameValid(nameTxt,((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)));
            boolean email= emailValid(emailTxt,((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)));
            if (name && email && !all.isSelected()) {//the booleans must be pulled seperately due to the way java does and statements.(if first is false, doesnt bother running second)
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(true);
            }
        });
         ((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)).textProperty().addListener((observable) -> {
            String emailTxt = ((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)).getText();
            String nameTxt = ((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)).getText();
            boolean name= nameValid(nameTxt,((TextField) gui.getGUINode(csg_NAME_TEXT_FIELD)));
            boolean email= emailValid(emailTxt,((TextField) gui.getGUINode(csg_EMAIL_TEXT_FIELD)));
            if (name && email&& !all.isSelected()) { //the booleans must be pulled seperately due to the way java does and statements. 
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(false);
            }
            else {
                ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setDisable(true);
            }
        });
        ((Button) gui.getGUINode(csg_ADD_TA_BUTTON)).setOnAction(e -> {
            if (((RadioButton) gui.getGUINode(csg_TAS_TYPE_G)).isSelected())
                controller.processAddTA("graduate");
            else controller.processAddTA("undergraduate");
        });
        TableView officeHoursTableView = (TableView) gui.getGUINode(csg_OFFICE_HOURS_TABLE_VIEW);
        // DON'T LET ANYONE SORT THE TABLES
        for (int i = 0; i < officeHoursTableView.getColumns().size(); i++) {
            ((TableColumn)officeHoursTableView.getColumns().get(i)).setSortable(false);
        }
        TableView tasTable = (TableView)gui.getGUINode(csg_TAS_TABLE_VIEW);
        tasTable.setOnMouseClicked(e->{
            CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
            if (check.isTASelected()) {  
               ((Button)gui.getGUINode(COPY_BUTTON)).setDisable(false);
               ((Button) gui.getGUINode(CUT_BUTTON)).setDisable(false);
            }
            else {
               ((Button)gui.getGUINode(COPY_BUTTON)).setDisable(true);
               ((Button) gui.getGUINode(CUT_BUTTON)).setDisable(true);               
            }
        });
        
        //removeTA button controller
        ((Button)gui.getGUINode(csg_oh_removeTA_button)).setOnAction(e->{
            CourseSiteGeneratorData checker = (CourseSiteGeneratorData)app.getDataComponent();
            TeachingAssistantPrototype ta = checker.getSelectedTA();
            ta = checker.getTAWithName(ta.getName());
            checker.removeTA(ta);
        });
         
         TableView officeHoursTable = (TableView)gui.getGUINode(csg_OFFICE_HOURS_TABLE_VIEW);
         officeHoursTable.setOnMouseClicked(e->{
            CourseSiteGeneratorData checker = (CourseSiteGeneratorData)app.getDataComponent();
            if (officeHoursTable.getSelectionModel().getSelectedCells().size()==0) return;
            TablePosition selected = (TablePosition)officeHoursTable.getSelectionModel().getSelectedCells().get(0);
            
            TableView taTable = (TableView)gui.getGUINode(csg_TAS_TABLE_VIEW);
            
            
            int rowNum = selected.getRow();
            int colNum = selected.getColumn();
            TableColumn startTimeCol = (TableColumn) officeHoursTable.getColumns().get(0);
            TableColumn endTimeCol = (TableColumn) officeHoursTable.getColumns().get(1);
            String startTime = startTimeCol.getCellData(rowNum).toString();
            startTime = startTime.replace(":","_");
            TimeSlot slot = checker.getTimeSlot(startTime);
            if (!checker.isDayOfWeekColumn(colNum)) return;
            String day = checker.getColumnDayOfWeek(colNum).name();
            if (checker.isTASelected()) { //only run if a TA is selected
                day = day.toLowerCase();
                TeachingAssistantPrototype ta = checker.getSelectedTA();
                boolean add;
                if (slot.taInSlot(day, ta)!=null) {
                     add = false;
                }
                else  {
                     add = true;
                }
                AddOH_Transaction addOHTransaction = new AddOH_Transaction(checker,slot, day, ta, add); //adds to all ta list
                app.processTransaction(addOHTransaction);
                TeachingAssistantPrototype ta2 = checker.getTAWithName(ta.getName());
                //ta2.setTimeslot(ta.getTimeslot());
                officeHoursTable.layout();
                checker.hightLightTACells(ta);
            }
            resizecsg();
//                    for (int i = 0; i < officeHoursTable.getColumns().size(); i++) {
//                 ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/6.0));
//                 ((TableColumn)officeHoursTable.getColumns().get(i)).prefWidthProperty().bind(officeHoursTable.widthProperty().multiply(1.0/7.0));
//        }          
        });
         
         ((Button)gui.getGUINode(csg_schedule_add_button)).setOnAction(e->{
               ComboBox type = (ComboBox)gui.getGUINode(csg_schedule_type_combo);
               DatePicker date = (DatePicker)gui.getGUINode(csg_schedule_date_date);
               TextField title =  (TextField)gui.getGUINode(csg_schedule_title_textfield);
               TextField topic =  (TextField)gui.getGUINode(csg_schedule_topic_textfield);
               TextField link =  (TextField)gui.getGUINode(csg_schedule_link_textfield);
               if(type.getValue()!=null&& date.getValue()!=null && topic.getText()!=null && title.getText()!=null && link.getText()!=null) {
                   controller.processAddScheudleItem();
               }
               
         
         });
      
         //removeTA transaction
         ((Button)gui.getGUINode(csg_oh_removeTA_button)).setOnAction(e->{
        CourseSiteGeneratorData checker = (CourseSiteGeneratorData)app.getDataComponent();
        TeachingAssistantPrototype ta = checker.getSelectedTA();
  
        String type = ta.getType();
        if (type.equals(((RadioButton)gui.getGUINode(csg_TAS_TYPE_UG)).getText())) {
            type = "undergraduate";
        }
        if (type.equals(((RadioButton)gui.getGUINode(csg_TAS_TYPE_G)).getText())) {
            type = type = "graduate";
        }
        AddCut_Transaction cut = new AddCut_Transaction(checker,ta,type);
        app.processTransaction(cut);
       });
         
         
        //meeting time 
         Button lecAddButton = (Button)gui.getGUINode(csg_meeting_lectures_add_button);
         Button recAddButton = (Button)gui.getGUINode(csg_meeting_rec_add_button);
         Button labAddButton = (Button)gui.getGUINode(csg_meeting_lab_add_button);
         
         
         //add new meeting times to list
         lecAddButton.setOnAction(e->{
              CourseSiteGeneratorData dataAdder = (CourseSiteGeneratorData)app.getDataComponent();
              dataAdder.addLec(new MeetingTime("lecture"));
         });
         recAddButton.setOnAction(e->{
              CourseSiteGeneratorData dataAdder = (CourseSiteGeneratorData)app.getDataComponent();
              dataAdder.addRec(new MeetingTime("recitation"));
         });
         labAddButton.setOnAction(e->{
              CourseSiteGeneratorData dataAdder = (CourseSiteGeneratorData)app.getDataComponent();
              dataAdder.addLab(new MeetingTime("lab"));
         });
         Button lecRemoveButton = (Button)gui.getGUINode(csg_meeting_lectures_remove_button);
         Button recRemoveButton = (Button)gui.getGUINode(csg_meeting_rec_remove_button);
         Button labRemoveButton = (Button)gui.getGUINode(csg_meeting_lab_remove_button);   
         
         lecRemoveButton.setOnAction(e->{
              CourseSiteGeneratorData dataRemove = (CourseSiteGeneratorData)app.getDataComponent();
              dataRemove.removeSelectedLec();
         });
        recRemoveButton.setOnAction(e->{
              CourseSiteGeneratorData dataRemove = (CourseSiteGeneratorData)app.getDataComponent();
              dataRemove.removeSelectedRec();
         });
        labRemoveButton.setOnAction(e->{
              CourseSiteGeneratorData dataRemove = (CourseSiteGeneratorData)app.getDataComponent();
              dataRemove.removeSelectedLab();
         });
        
        ArrayList<TableView<MeetingTime>> meetings = new ArrayList<TableView<MeetingTime>>(3);
        
        
        meetings.add((TableView<MeetingTime>)gui.getGUINode(csg_meeting_lectures_table));
        meetings.add((TableView<MeetingTime>)gui.getGUINode(csg_meeting_rec_table));
        meetings.add((TableView<MeetingTime>)gui.getGUINode(csg_meeting_lab_table));
        
       //meeting time data change iterative creation 
       for (int k=0; k<meetings.size(); k++) { 
            for (int i=0; i<meetings.get(k).getColumns().size(); i++) {
                for (int j=0; j<meetings.get(k).getColumns().get(i).getColumns().size(); j++) {
                    TableView<MeetingTime> meet = meetings.get(k);
                    TableColumn<MeetingTime,?> row = meet.getColumns().get(i);
                    
                    TableColumn col = row.getColumns().get(j);
                    meet.getColumns().get(i).getColumns().get(j).textProperty().addListener((observable,oldVal,newVal)->{
                        CourseSiteGeneratorData addingTrans = (CourseSiteGeneratorData)app.getDataComponent();
                        app.processTransaction(new AddEdit_Meeting_Transaction(addingTrans,col,oldVal,newVal));
                    });
                }
            }
       } 
        
        
         
         
      //Syllabus Edits
    
         
         
        
        
    //make textfields appear
    ((Button)gui.getGUINode(csg_site_addOh_button)).setOnAction(e-> {
        TextArea textArea = (TextArea)gui.getGUINode(csg_site_oh_textarea);
        textArea.setLayoutX(15);
        boolean visible = !textArea.visibleProperty().getValue(); //flip visibility.
        //removes visibility and item thing
        textArea.setVisible(visible); 
        textArea.setManaged(visible);
        if (visible==true) {
            csgBuilder.changeIcon((Button)gui.getGUINode(csg_site_addOh_button),csg_site_addOh_button,"MINUS", 15, 15);
            
        }
        else {
            csgBuilder.changeIcon((Button)gui.getGUINode(csg_site_addOh_button),csg_site_addOh_button,"PLUS", 15, 15);
        }
    });
            syllabusItems[] enums = syllabusItems.values();

            for (int i=0; i<syllabusItemPropertyNames.size(); i++) {
               syllabusItems currentEnum = enums[i];
                
                String name = syllabusItemPropertyNames.get(i);
                
                String textAreaId = "csg_syllabus_"+name+"_textarea";
                String buttonId = "csg_syllabus_"+name+"_button";
                ((Button)gui.getGUINode(buttonId)).setOnAction(e-> {
                    TextArea textArea = (TextArea)gui.getGUINode(textAreaId);
                    textArea.setLayoutX(15);
                    boolean visible = !textArea.visibleProperty().getValue(); //flip visibility.
                    //removes visibility and item thing
                    textArea.setVisible(visible); 
                    textArea.setManaged(visible);
                    if (visible==true) {
                        textArea.setPrefHeight(100);
                        csgBuilder.changeIcon((Button)gui.getGUINode(buttonId),buttonId,"MINUS", 15, 15);

                    }
                    else {
                        textArea.prefHeight(0);
                        csgBuilder.changeIcon((Button)gui.getGUINode(buttonId),buttonId,"PLUS", 15, 15);
                    }
            });
            ((TextArea)gui.getGUINode(textAreaId)).textProperty().addListener((observable, oldVal,newVal)->{
                if (((TextArea)gui.getGUINode(textAreaId)).isFocused()) {
                   CourseSiteGeneratorData syllabusGetter = (CourseSiteGeneratorData)app.getDataComponent();
                   if (syllabusGetter==null) return;
                   oldVal = syllabusGetter.getSyllabus().get(currentEnum).getValue();
                   int randomNum = (int)Math.random()*3+3 ;//randomly chooses a number between 3 and 6 as saving point
                   int oldValWords = oldVal.split(" ").length;
                   int newValWords = newVal.split(" ").length;
                   if (Math.abs(newVal.length()-oldVal.length())>=randomNum ||Math.abs(oldValWords-newValWords)>=2) { 
                        CourseSiteGeneratorData dataStuff = (CourseSiteGeneratorData) app.getDataComponent();
                        AddEdit_Syllabus_Transaction trans = new AddEdit_Syllabus_Transaction(dataStuff, currentEnum, oldVal, newVal);
                        app.processTransaction(trans);
                   }
               }
            });
        }
            tasTable = (TableView)gui.getGUINode(csg_TAS_TABLE_VIEW);
             tasTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent mouseEvent) {
        CourseSiteGeneratorData check = (CourseSiteGeneratorData)app.getDataComponent();
        if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
            if(mouseEvent.getClickCount() == 2){
                TeachingAssistantPrototype ta = check.getSelectedTA();
                ta = check.getTAWithName(ta.getName());
                if(ta!=null) {
                    ((TextField) gui.getGUINode(csg_EDIT_TA_NAME_TEXT_FIELD)).setText(ta.getName());
                    ((TextField) gui.getGUINode(csg_EDIT_TA_EMAIL_TEXT_FIELD)).setText(ta.getEmail());
                    String type = ta.getType();
                    if (type.equals("undergraduate")) {
                        RadioButton ug = ((RadioButton)gui.getGUINode(csg_EDIT_TA_UNDERGRADUATE_RADIO));
                        ug.getToggleGroup().selectToggle(ug);
                    }
                    else {
                        RadioButton g = ((RadioButton)gui.getGUINode(csg_EDIT_TA_GRADUATE_RADIO));
                        g.getToggleGroup().selectToggle(g);
                    }
                   taWindow.setTitle(((Label)gui.getGUINode(csg_EDIT_TA_LABEL)).getText());
                   taWindow.show();
                }       
        }
        }
    }
  });    
   //
   ((Label)gui.getGUINode(csg_site_export_dir_warning)).textProperty().addListener(e->{
       changeExportDir();
   });
   
   
   ((ComboBox)gui.getGUINode(csg_schedule_type_combo)).setEditable(true);
   
   ((ComboBox)gui.getGUINode(csg_schedule_type_combo)).getSelectionModel().selectedItemProperty().addListener(e->{
        String type = ((ComboBox)gui.getGUINode(csg_schedule_type_combo)).getEditor().getText();
       if (!typeOptions.contains(type)&&!type.equals("")) typeOptions.add(type);
   });
   
   //Long List of Site Tab controllers
   
   //combo transactions
   bannerCombos.add((ComboBox)gui.getGUINode(csg_site_css_combo));
   for (int i =0; i< bannerCombos.size(); i++) {
       ComboBox b = bannerCombos.get(i);
       b.valueProperty().addListener((observable,oldVal,newVal)->{
       if (!b.getItems().contains(newVal)) b.getItems().add(newVal);
       CourseSiteGeneratorData dataMan = (CourseSiteGeneratorData)app.getDataComponent();
       Node node = (Node)b;
       if (!node.isFocused()) return;
       ObservableList<String> list = ((ComboBox)node).getItems();
       int OldVal = -1;
       int NewVal = 0;
       for (int j = 0; j<list.size(); j++) {
           if (oldVal == (list.get(j))) {
               OldVal = j;
           } 
           if (newVal == list.get(j)) {
               NewVal = j;
           }
       }
       AddEdit_Site_Transaction trans  = new AddEdit_Site_Transaction(dataMan,node,"combobox",""+OldVal,""+NewVal);
       app.processTransaction(trans);
    });
   }
   
   //Textfields/Text area
   ((TextField)gui.getGUINode(csg_site_name_textfield)).textProperty().addListener((observable,oldVal,newVal)->{
       textChange(gui.getGUINode(csg_site_name_textfield),"textfield",oldVal,newVal);
   });
   ((TextField)gui.getGUINode(csg_site_room_textfield)).textProperty().addListener((observable,oldVal,newVal)->{
       textChange(gui.getGUINode(csg_site_room_textfield),"textfield",oldVal,newVal);
   });
   ((TextField)gui.getGUINode(csg_site_email_textfield)).textProperty().addListener((observable,oldVal,newVal)->{
       textChange(gui.getGUINode(csg_site_email_textfield),"textfield",oldVal,newVal);
   });
   ((TextField)gui.getGUINode(csg_site_homepage_textfield)).textProperty().addListener((observable,oldVal,newVal)->{
       textChange(gui.getGUINode(csg_site_homepage_textfield),"textfield",oldVal,newVal);
   });
   ((TextField)gui.getGUINode(csg_site_title_textfield)).textProperty().addListener((observable,oldVal,newVal)->{
       textChange(gui.getGUINode(csg_site_title_textfield),"textfield",oldVal,newVal);
   });
   ((TextArea)gui.getGUINode(csg_site_oh_textarea)).textProperty().addListener((observable,oldVal,newVal)->{
       textChange(gui.getGUINode(csg_site_oh_textarea),"textarea",oldVal,newVal);
   });
      
   //Images
   ImageView fav = (ImageView)((Button)gui.getGUINode(csg_site_favicon_image)).getGraphic();
   ImageView nav = (ImageView)((Button)gui.getGUINode(csg_site_navbar_image)).getGraphic();
   ImageView left = (ImageView)((Button)gui.getGUINode(csg_site_leftFooter_image)).getGraphic();
   ImageView right = (ImageView)((Button)gui.getGUINode(csg_site_rightFooter_image)).getGraphic();
   
   
   
  
   
}
    public void textChange(Node node,String type, String oldVal, String newVal) {
       CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
       if (node.isFocused()) {
            AddEdit_Site_Transaction trans = new AddEdit_Site_Transaction(data,node,type,oldVal,newVal);
            app.processTransaction(trans);
       }
    }

    private void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(csg_FOOLPROOF_SETTINGS,
                new CourseSiteGeneratorFoolproofDesign((CourseSiteGeneratorApp) app));
    }
    public boolean nameValid(String name, TextField field) {
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData checker = (CourseSiteGeneratorData)app.getDataComponent();

        TeachingAssistantPrototype ta= checker.getTAWithName(name);
        if (ta!=null) {
//            checker.selectTA(name,0);
            field.setStyle("-fx-text-inner-color: red;");
            return false;
        }
        if (name.length()>1) {
            field.setStyle("-fx-text-inner-color: black;");
            return true;
        }
        field.setStyle("-fx-text-inner-color: red;");
        return false;
    }
    public boolean emailValid(String email, TextField field) {
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData checker = (CourseSiteGeneratorData)app.getDataComponent();

        TeachingAssistantPrototype ta= checker.getTAWithEmail(email);
        if (ta!=null) {
//            checker.selectTA(email,1);
             field.setStyle("-fx-text-inner-color: red;");
             return false;
        }
        Pattern p = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(email);
        if (m.find()) {
            field.setStyle("-fx-text-inner-color: black;");
            return true;
        }
        field.setStyle("-fx-text-inner-color: red;");
        return false;
    }
   public static void saveImage(Image image, String name) {
    File outputFile = new File("images\\"+name);
    BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
    try {
      ImageIO.write(bImage, "png", outputFile);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  } 
    
    
    public void switchToType(String type) {
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData checker = (CourseSiteGeneratorData)app.getDataComponent();
        checker.switchToType(type);
    }

    @Override
    public void showNewDialog() {
    }
    public void resizecsg() {
        AppGUIModule gui = app.getGUIModule();
        TableView<TimeSlot> CourseSiteGeneratorTable= ((TableView)gui.getGUINode(csg_OFFICE_HOURS_TABLE_VIEW));
         for (int i = 0; i < CourseSiteGeneratorTable.getColumns().size(); i++) {
                 ((TableColumn)CourseSiteGeneratorTable.getColumns().get(i)).prefWidthProperty().bind(CourseSiteGeneratorTable.widthProperty().multiply(1.0/6.0));
                 ((TableColumn)CourseSiteGeneratorTable.getColumns().get(i)).prefWidthProperty().bind(CourseSiteGeneratorTable.widthProperty().multiply(1.0/7.0));
        } 
    }
    public void updateCSSFiles() {
        cssFiles.clear();
        File folder = new File("work\\css\\");
        File[] listOfFiles = folder.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                cssFiles.add(file.getName());
            }
        }
    }
        public void taChange() {
        AppGUIModule gui = app.getGUIModule();
        CourseSiteGeneratorData checker = (CourseSiteGeneratorData)app.getDataComponent();
        if (checker.isTASelected()) {
            checker.hightLightTACells(checker.getSelectedTA());
        }
    }
   public void changeExportDir() {
       AppGUIModule gui = app.getGUIModule();
       String sub = ((ComboBox)gui.getGUINode(csg_site_subject_combo)).getEditor().getText();
       if (!subjects.contains(sub) && !sub.equals("") && !sub.isEmpty()) subjects.add(sub);
       String num =((ComboBox)gui.getGUINode(csg_site_number_combo)).getEditor().getText();
       if (!numbers.contains(num) && !num.equals("") && !num.isEmpty()) numbers.add(sub);
       String sem = ((ComboBox)gui.getGUINode(csg_site_semester_combo)).getEditor().getText();
       if (!semesters.contains(sem)&& !sem.equals("") && !sem.isEmpty()) semesters.add(sem);
       String year = ((ComboBox)gui.getGUINode(csg_site_year_combo)).getEditor().getText();
       if (!years.contains(year)&& !year.equals("") && !year.isEmpty()) years.add(year);
       Label exportDir = (Label)gui.getGUINode(csg_site_exportVal_label);
       String dirText = ".\\export\\" +sub+"_"+num+"_"+sem+"_"+year+"\\public.html";
       
       boolean exportEnabled = ((CheckBox)gui.getGUINode(csg_site_home_checkbox)).isSelected();
       exportEnabled = exportEnabled || ((CheckBox)gui.getGUINode(csg_site_syllabus_checkbox)).isSelected();
       exportEnabled = exportEnabled || ((CheckBox)gui.getGUINode(csg_site_schedule_checkbox)).isSelected();
       exportEnabled = exportEnabled || ((CheckBox)gui.getGUINode(csg_site_hws_checkbox)).isSelected();
       exportEnabled = exportEnabled &&(!sub.isEmpty() && !num.isEmpty() && !sem.isEmpty() && !years.isEmpty());
       if (exportEnabled) exportDir.setText(dirText);
       else exportDir.setText(((Label)gui.getGUINode(csg_site_export_dir_warning)).getText());
   }
   public void openImageDialog(String buttonName) {
       AppGUIModule gui = app.getGUIModule();
       AppNodesBuilder csgBuilder = gui.getNodesBuilder();
       CourseSiteGeneratorData data = (CourseSiteGeneratorData)app.getDataComponent();
       
      fileChooser.getExtensionFilters().addAll(
           new FileChooser.ExtensionFilter("All Images", "*.*"),
           new FileChooser.ExtensionFilter("JPG", "*.jpg"),
           new FileChooser.ExtensionFilter("PNG", "*.png")
      );
      File file = fileChooser.showOpenDialog(gui.getWindow());
      if (file != null) {
         String path = file.getPath();
         String imageViewId = "csg_site_"+buttonName+"_image";
         
         String oldImage = "";
         if(buttonName.equals("favicon")) { 
             oldImage = faviconPath;
             faviconPath = file.getName();
         }
         else if(buttonName.equals("navbar")) {oldImage = navBarPath; navBarPath = file.getName();}
         else if(buttonName.equals("leftFooter")){ oldImage = leftFooterPath; leftFooterPath = file.getName();}
         else {oldImage = rightFooterPath; rightFooterPath = file.getName();}
         
         Image im = new Image("file:"+path);
         saveImage(im,file.getName());
         
         AddEdit_Site_Transaction trans = new AddEdit_Site_Transaction(data,(Button)gui.getGUINode(imageViewId), "image",oldImage,file.getName());
         app.processTransaction(trans);
        // data.openImage(file.getName(),(Button)gui.getGUINode(imageViewId));
//         ImageView imageView = (ImageView)((Button)gui.getGUINode(imageViewId)).getGraphic();
//         imageView.setImage(im);
         
      }
   }
 public void addToTimes(String time) {
     timesStart.add(time);
 }
  public String getFavicon() {
     return faviconPath;
 }
 public String getNavBar() {
     return navBarPath;
 }
    public String getLeft() {
     return leftFooterPath;
 }
    public String getRight() {
     return rightFooterPath;
 }
}
